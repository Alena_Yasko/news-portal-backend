**News Management project provides backend for a news portal.**

   **Functionality:**
    -add news
    -edit news
    -delete news
    -view the list of news (pagination)
    -view single news item
    -add news author
    -search news according to search criteria (author, tags)
    -add tag(s) for a news item
    -add comment(s) for a news item
    -delete comment(s)
    -sort news by creation date and by most commented news
    -count all news
    -expire author(s)
 **

**Technology stack:**
-Maven build tool
-Mokito and DBUnit for testing
-Spring DI
-AspectJ
-Logback

**
Database Installation:**
**1.** Install Oracle Database Express Edition 11g Release 2 (http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)
**2.** Create user "NEWS" with password "password". Use "news-common\src\test\dbscript\ddl\create_db.sql" script to create db, and "news-common\src\test\dbscript\dml\fill_db.sql" to fill it with test data.
**3.** Create user "TEST" with password "password" for dbunit tests.

**Building:**
Run "mvn package" in command line to create jar.