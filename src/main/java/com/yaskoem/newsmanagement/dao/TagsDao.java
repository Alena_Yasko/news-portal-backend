package com.yaskoem.newsmanagement.dao;

import com.yaskoem.newsmanagement.domain.Tag;

import java.util.List;

/**
 * TagsDao is used to get/put tags-related data from or into data source.
 * @author Alena Yasko
 */
public interface TagsDao {

	/**
	 * Adds a new record to tags data source.
	 *
	 * @param tag an object that represents a new record to be added.
	 * @return id of the inserted record.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
   	long add(Tag tag);

	/**
	 * Creates a relation between a tag and a news record.
	 *
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
    void bindToNews(long newsId, long tagId);

	/**
	 * Loads a tag from data source.
	 *
	 * @param id id of the tag to be loaded.
	 * @return loaded tag or <tt>null</tt> if the id was not found.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
    Tag load(long id);

	/**
	 * Finds a list of tags related to news with the specified id.
	 *
	 * @param newsId id of the news related to the requested author.
	 * @return loaded list of tags. If no tags related to the news were found an empty list is returned.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
    List<Tag> loadByNewsId(long newsId);

	/**
	 * Loads all tags from data source.
	 * @return list of loaded tags or an empty list if no tags were found.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
	List<Tag> loadAll();

    void unbindFromNews(long newsId, long tagId);

	/**
	 * Finds all records of relations between a tag with the specified id and news and removes them.
	 *
	 * @param tagId <tt>id</tt> of the tag that is to be unbound from all news records.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
    void unbindFromAllNews(long tagId);

	/**
	 * Finds all records of relations between a news with the specified id and tags and removes them.
	 *
	 * @param newsId <tt>id</tt> of the news that is to be unbound from all tags.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
    void unbindAllFromNews(long newsId);

	/**
	 * Updates a tag with the id equal to parameter's <tt>id</tt> field.
	 * New data for the update is retrieved from parameter's fields.
	 *
	 * @param tag Tag object containing new values. <tt>id</tt> field should not be null.
	 * @throws com.yaskoem.newsmanagement.exception.DaoException
	 */
    void update(Tag tag);

	void delete(Long tagId);
}