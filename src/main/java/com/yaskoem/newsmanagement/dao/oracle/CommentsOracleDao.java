package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.CommentsDao;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.exception.DaoException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * CommentsOracleDao operates on COMMENTS table in Oracle DB to get data.
 * @author Alena Yasko
 */
public class CommentsOracleDao extends BaseOracleDao implements CommentsDao {

	/** SQL for inserting comment */
	private static final String SQL_INSERT_COMMENT =
        "INSERT INTO comments (cm_news_id_fk, cm_comment_text, cm_creation_date) VALUES (?, ?, CURRENT_TIMESTAMP)";
	
	/** SQL for deleting comment */
	private static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE cm_comment_id_pk = ?";
	
	/** SQL for selecting comments by news id */
	private static final String SQL_SELECT_NEWS_COMMENTS =
        "SELECT cm_comment_id_pk, cm_news_id_fk, cm_comment_text, cm_creation_date FROM comments WHERE cm_news_id_fk = ?";
	
	/** SQL for deleting comment by news id */
	private static final String SQL_DELETE_NEWS_COMMENTS = "DELETE FROM comments WHERE cm_news_id_fk = ?";
	
	/** PK column name in comments table */
	private static final String COMMENTS_PK_COLUMN = "cm_comment_id_pk";

    /**
     * {@inheritDoc}
     */
    @Override
    public Long add(Comment comment) {
        
    	long commentId;
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_INSERT_COMMENT, COMMENTS_PK_COLUMN);

            preparedStatement.setLong(1, comment.getNewsId());
            preparedStatement.setString(2, comment.getText());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            commentId = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with comment=%s", comment), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return commentId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(long commentId) {

        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_DELETE_COMMENT);

            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with commentId=%d", commentId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAllByNewsId(long newsId) {
       
    	PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_DELETE_NEWS_COMMENTS);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Comment> loadByNewsId(long newsId) {
        
    	List<Comment> comments = new ArrayList<>();
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_SELECT_NEWS_COMMENTS);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                comments.add(getCommentFromResultSet(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return comments;
    }

    /**
     * Creates a Comment instance and sets its fields with values from query result.
     * @param resultSet The returned result set.
     * @return Created Comment instance.
     */
    private Comment getCommentFromResultSet(ResultSet resultSet) throws SQLException {
    	Long commentId = resultSet.getLong(1);
        Long newsId = resultSet.getLong(2);
        String text = resultSet.getString(3);
    	Date creationDate = resultSet.getTimestamp(4);

        return new Comment(commentId, newsId, text, creationDate);
    }
}