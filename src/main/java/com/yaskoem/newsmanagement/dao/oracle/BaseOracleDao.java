package com.yaskoem.newsmanagement.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.yaskoem.newsmanagement.dao.DaoUtils;

/**
 * Base class for data access object using database as data source.
 * 
 * @author Alena_Yasko
 */
public abstract class BaseOracleDao {

	/** Data source */
	@Autowired
	private DataSource dataSource;
	
	/** Database connection */
	private Connection connection = null;
	
	/**
	 * Establishes connection and creates PreparedStatement instance for the given SQL.
	 * @param sql SQL string.
	 * @return PreparedStatement instance.
	 * @throws SQLException In case of an error.
	 */
    public PreparedStatement connectAndPrepareStatement(String sql) throws SQLException {
        connection = DataSourceUtils.doGetConnection(dataSource);
        return connection.prepareStatement(sql);
    }
    
	/**
	 * Establishes connection and creates PreparedStatement instance for the given INSERT SQL.
	 * @param sql INSERT SQL string.
	 * @param pkColumn PK column name for PK generation.
	 * @return PreparedStatement instance.
	 * @throws SQLException In case of an error.
	 */
    public PreparedStatement connectAndPrepareStatement(String sql, String pkColumn) throws SQLException {
        connection = DataSourceUtils.doGetConnection(dataSource);
        return connection.prepareStatement(sql, new String[] { pkColumn });
    }
    
    /**
     * Closes the given statement and disconnects.
     * @param statement The Statement instance to close.
     */
    public void closeStatementAndDisconnect(Statement statement) {
        DaoUtils.closeQuietly(statement);
        DaoUtils.releaseQuietly(connection, dataSource);
        connection = null;
    }
}