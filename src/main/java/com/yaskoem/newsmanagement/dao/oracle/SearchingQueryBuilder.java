package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.domain.SearchCriteria;
import org.springframework.util.Assert;

/**
 * SearchingQueryBuilder is a util class for building complex searching queries
 * based on a value of SearchCriteria object's fields.
 * @author Alena Yasko
 */
class SearchingQueryBuilder {

	/**
     * A query to retrieve all news from database. It is wrapped into below outer queries
     * to narrow the results as requested.
     */
    private static final String SQL_SELECT_BY_CRITERIA = "SELECT DISTINCT ns_news_id_pk, ns_title, ns_short_text, " +
        "ns_full_text, ns_creation_date, ns_modification_date FROM news " +
        "LEFT JOIN news_authors ON na_news_id_fk = ns_news_id_pk " +
        "LEFT JOIN news_tags ON nt_news_id_fk = ns_news_id_pk";

	/**
     * A wrapper query for sorting results of a sub-query by modification date
     * and number of comments.
     */
    private static final String SQL_SORT_TEMPLATE = "SELECT ns_news_id_pk, ns_title, ns_short_text, " +
        "ns_full_text, ns_creation_date, ns_modification_date, row_number() OVER (ORDER BY " +
            "TO_DATE(ns_modification_date) DESC, COUNT(cm_news_id_fk) DESC) rowNumber FROM (%s) " +
            "LEFT JOIN comments ON ns_news_id_pk = cm_news_id_fk " +
        "GROUP BY ns_news_id_pk, ns_title, ns_short_text, ns_full_text, ns_creation_date, ns_modification_date";

    /**
     * A wrapper query that counts news in a result of a sub-query.
     */
    private static final String SQL_COUNT_FILTERED_NEWS_TEMPLATE = "SELECT COUNT(*) FROM(%s)";

	/**
     * A wrapper query that crops a result of its sub-query to the specified range of rows.
     */
    private static final String SQL_NEWS_RANGE_SELECTION_TEMPLATE = "SELECT ns_news_id_pk, ns_title, ns_short_text, " +
        "ns_full_text, ns_creation_date, ns_modification_date, rowNumber FROM (SELECT ns_news_id_pk, ns_title, " +
        "ns_short_text, ns_full_text, ns_creation_date, ns_modification_date, rowNumber " +
        "FROM (%s) resList WHERE ROWNUM <= ?) WHERE rowNumber >= ?";

	/**
     * A wrapper query to select left or right neighbour of a news with the specified id
     * from a result of a sub-query.
     */
    private static final String SQL_NEIGHBOUR_NEWS_SELECTION_TEMPLATE = "SELECT ns_news_id_pk, ns_title, ns_short_text, " +
        "ns_full_text, ns_creation_date, ns_modification_date FROM news WHERE ns_news_id_pk = (SELECT neighbourId " +
        "FROM (SELECT ns_news_id_pk, %s(ns_news_id_pk) OVER (ORDER BY rowNumber ASC) AS neighbourId FROM (%s)) " +
        "WHERE ns_news_id_pk = ?)";

    /***/
    private static final String SQL_WHERE = " WHERE ";

    /***/
    private static final String SQL_AND = " AND ";

    /***/
    private static final String SQL_AUTHOR_ID_EQUALS_TEMPLATE = "na_author_id_fk = %d";

    /***/
    private static final String SQL_TAG_ID_IN = "nt_tag_id_fk IN (";

    /***/
    private static final String SQL_LAG = "LAG";

    /***/
    private static final String SQL_LEAD = "LEAD";

	/**
     * Builds a query for getting a range of sorted news that meet the requirements
     * of a <tt>searchCriteria</tt> parameter.
     *
     * @param searchCriteria
     * @return the result query with <tt>question marks</tt> in places of range borders for <tt>PreparedStatement</tt>.
     */
    public static String buildForRange(SearchCriteria searchCriteria) {

        String queryForCriteria = buildForCriteria(searchCriteria);

        return String.format(SQL_NEWS_RANGE_SELECTION_TEMPLATE, String.format(SQL_SORT_TEMPLATE, queryForCriteria));
    }

    /**
     * Builds a query for getting previous neighbour of a news with the specified id from sorted list of
     * news that meet the requirements of Searching criteria.
     *
     * @param searchCriteria
     * @return the result query with a <tt>question mark</tt> in place of news id.
     */
    public static String buildForPrevious(SearchCriteria searchCriteria) {
        String queryForCriteria = buildForCriteria(searchCriteria);

        return String.format(SQL_NEIGHBOUR_NEWS_SELECTION_TEMPLATE, SQL_LAG, String.format(SQL_SORT_TEMPLATE,
                queryForCriteria));
    }

    /**
     * Builds a query for getting next neighbour of a news with the specified id from sorted list of
     * news that meet the requirements of Searching criteria.
     *
     * @param searchCriteria
     * @return the result query with a <tt>question mark</tt> in place of news id.
     */
    public static String buildForNext(SearchCriteria searchCriteria) {
        String queryForCriteria = buildForCriteria(searchCriteria);

        return String.format(SQL_NEIGHBOUR_NEWS_SELECTION_TEMPLATE, SQL_LEAD, String.format(SQL_SORT_TEMPLATE,
            queryForCriteria));
    }

	/**
     * Builds a query for counting news that meet the requirements of <tt>searchCriteria</tt> parameter.
     * @param searchCriteria
     * @return result query.
     */
    public static String buildForCount(SearchCriteria searchCriteria) {
        String queryForCriteria = buildForCriteria(searchCriteria);

        return String.format(SQL_COUNT_FILTERED_NEWS_TEMPLATE, queryForCriteria);
    }

	/**
     * Builds a criteria for getting all news that meet the requirements of <tt>searchCriteria</tt> parameter.
     * @param searchCriteria non-null object that specifies what news to select.
     *                       If <tt>authorId</tt> field is <tt>null</tt>, news of any author will be searched for.
     *                       If <tt>tagsList</tt> is <tt>null</tt> or empty, news of any tags will be searched for.
     * @return result query.
     */
    public static String buildForCriteria(SearchCriteria searchCriteria) {
        Assert.notNull(searchCriteria, "searchCriteria must not be null.");

        StringBuilder query = new StringBuilder();

        query.append(SQL_SELECT_BY_CRITERIA);

        if (searchCriteria.getAuthorId() != null || (searchCriteria.getTagsIds() != null &&
                !searchCriteria.getTagsIds().isEmpty())) {
            query.append(SQL_WHERE);
        }
        if (searchCriteria.getAuthorId() != null) {
            query.append(String.format(SQL_AUTHOR_ID_EQUALS_TEMPLATE, searchCriteria.getAuthorId()));
        }
        if (searchCriteria.getTagsIds() != null && !searchCriteria.getTagsIds().isEmpty()) {
            query = searchCriteria.getAuthorId() != null ? query.append(SQL_AND + SQL_TAG_ID_IN) :
                query.append(SQL_TAG_ID_IN);

            for (long tagId : searchCriteria.getTagsIds()) {
                query.append(tagId);
                query.append(", ");
            }
            query.replace(query.length() - 2, query.length(), ")");
        }

        return query.toString();
    }
}