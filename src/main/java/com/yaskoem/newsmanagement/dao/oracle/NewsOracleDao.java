package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.NewsDao;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.exception.DaoException;
import com.yaskoem.newsmanagement.domain.News;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * NewsOracleDao operates on NEWS and NEWS_* tables in Oracle DB to get news-related data.
 * @see NewsDao
 * @author Alena Yasko
 */
public class NewsOracleDao extends BaseOracleDao implements NewsDao {

	/** SQL for inserting news */
    private static final String SQL_INSERT_NEWS = "INSERT INTO news (ns_title, ns_short_text, ns_full_text, " +
        "ns_creation_date, ns_modification_date) VALUES (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_DATE)";

	/** SQL for updating news */
    private static final String SQL_UPDATE_NEWS =
        "UPDATE news SET ns_title = ?, ns_short_text = ?, ns_full_text = ?, ns_modification_date = ? WHERE ns_news_id_pk = ?";

	/** SQL for selecting news by id */
    private static final String SQL_SELECT_NEWS_BY_ID = "SELECT ns_news_id_pk, ns_title, ns_short_text, ns_full_text, "
        + "ns_creation_date, ns_modification_date FROM news WHERE ns_news_id_pk = ?";

	/** SQL for deleting news */
    private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE ns_news_id_pk = ?";

    /** PK column name in news table */
    private static final String NEWS_PK_COLUMN = "ns_news_id_pk";
    
    /**
     * {@inheritDoc}
     */
    @Override
    public long add(News news) {
    	
        long newsId;
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_INSERT_NEWS, NEWS_PK_COLUMN);

            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            newsId = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with news=%s", news), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return newsId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(News news) {

        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_UPDATE_NEWS);

            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setDate(4, new java.sql.Date(news.getModificationDate().getTime()));
            preparedStatement.setLong(5, news.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with news=%s", news), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public News load(long newsId) {
        return selectByQuery(SQL_SELECT_NEWS_BY_ID, newsId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> loadList(SearchCriteria searchCriteria, int firstNewsNumber, int maxCount) {
     
    	List<News> newsList = new ArrayList<>();
        String query = SearchingQueryBuilder.buildForRange(searchCriteria);
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(query);

            preparedStatement.setInt(1, firstNewsNumber + maxCount - 1);
            preparedStatement.setInt(2, firstNewsNumber);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                newsList.add(getNewsFromResultSet(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with searchCriteria=%s", searchCriteria), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return newsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(long newsId) {

        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_DELETE_NEWS);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long count(SearchCriteria searchCriteria) {
       
    	long numberOfNews;
    	String query = SearchingQueryBuilder.buildForCount(searchCriteria);
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(query);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            numberOfNews = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return numberOfNews;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public News loadPrevious(SearchCriteria searchCriteria, long newsId) {
        return selectByQuery(SearchingQueryBuilder.buildForPrevious(searchCriteria), newsId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public News loadNext(SearchCriteria searchCriteria, long newsId) {
        return selectByQuery(SearchingQueryBuilder.buildForNext(searchCriteria), newsId);
    }

    private News selectByQuery(String query, long newsId) {
        
    	News news = null;
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(query);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                news = getNewsFromResultSet(resultSet);
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Exception occurred with newsId=%d", newsId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return news;
    }

    /**
     * Creates a News instance and sets its fields with values from query result.
     * @param resultSet The returned result set.
     * @return Created News instance.
     */
    private News getNewsFromResultSet(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong(1);
        String title = resultSet.getString(2);
        String shortText = resultSet.getString(3);
        String fullText = resultSet.getString(4);
        Date creationDate = resultSet.getTimestamp(5);
        Date modificationDate = resultSet.getDate(6);

        return new News(id, title, shortText, fullText, creationDate, modificationDate);
    }
}