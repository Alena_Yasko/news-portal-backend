package com.yaskoem.newsmanagement.dao.oracle;

import com.yaskoem.newsmanagement.dao.AuthorsDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.exception.DaoException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AuthorsOracleDao operates on AUTHORS and NEWS_AUTHORS tables in Oracle DB to get data.
 * @author Alena Yasko
 */
public class AuthorsOracleDao extends BaseOracleDao implements AuthorsDao {

	/** SQL for inserting author */
    private static final String SQL_INSERT_AUTHOR = "INSERT INTO authors(au_author_name_uq) VALUES (?)";

    /** SQL for adding news-author record */
    private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO news_authors (na_news_id_fk, na_author_id_fk) VALUES (?, ?)";

    /** SQL for deleting news-author record by news id - unbind author from the news */
    private static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM news_authors WHERE na_news_id_fk = ?";

    /** SQL for selecting author by news id */
    private static final String SQL_SELECT_NEWS_AUTHOR = "SELECT au_author_id_pk, au_author_name_uq, au_expired FROM authors " +
        "INNER JOIN news_authors ON au_author_id_pk = na_author_id_fk WHERE na_news_id_fk = ?";

    /** SQL for selecting author by id */
    private static final String SQL_SELECT_AUTHOR_BY_ID =
        "SELECT au_author_id_pk, au_author_name_uq, au_expired FROM authors WHERE au_author_id_pk = ?";

    /** SQL for selecting non-expired authors */
    private static final String SQL_SELECT_NOT_EXPIRED_AUTHORS =
        "SELECT au_author_id_pk, au_author_name_uq, au_expired FROM authors WHERE au_expired IS NULL";

    /** SQL for updating author */
    private static final String SQL_UPDATE_AUTHOR =
        "UPDATE authors SET au_author_name_uq = ?, au_expired = ? WHERE au_author_id_pk = ?";
    
    /** PK column name in authors table */
    private static final String AUTHORS_PK_COLUMN = "au_author_id_pk";

    /**
     * {@inheritDoc}
     */
    @Override
    public long add(Author author) {
    	
    	long authorId;
    	PreparedStatement preparedStatement = null;
    	
    	try {
        	preparedStatement = connectAndPrepareStatement(SQL_INSERT_AUTHOR, AUTHORS_PK_COLUMN);
            preparedStatement.setString(1, author.getName());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            authorId = resultSet.getLong(1);
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't add author '%s'", author), e);
        }
        finally {
            closeStatementAndDisconnect(preparedStatement);
        }

        return authorId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Author author) {
    	
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_UPDATE_AUTHOR);
            preparedStatement.setString(1, author.getName());

            if (author.getExpired() != null) {
                preparedStatement.setTimestamp(2, new Timestamp(new Date().getTime()));
            }
            else {
                preparedStatement.setNull(2, Types.TIMESTAMP);
            }

            preparedStatement.setLong(3, author.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't update author '%s'", author), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bindToNews(long newsId, long authorId) {
       
    	PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_ADD_NEWS_AUTHOR);

            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, authorId);
            preparedStatement.execute();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't bind news '%s' to author '%s'", newsId, authorId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unbindFromNews(long newsId) {
        
    	PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_DELETE_NEWS_AUTHOR);

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't unbind author from news '%s'", newsId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Author loadByNewsId(long newsId) {
        
    	Author author = null;
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_SELECT_NEWS_AUTHOR);

            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                author = getAuthorFromResultSet(resultSet);
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't load author by newsId='%s'", newsId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }
        
        return author;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Author load(long authorId) throws DaoException {
        
    	Author author = null;
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_SELECT_AUTHOR_BY_ID);

            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                author = getAuthorFromResultSet(resultSet);
            }
        }
        catch (SQLException e) {
            throw new DaoException(String.format("Can't load author with id '%s'", authorId), e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return author;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Author> loadNotExpired() throws DaoException {
        
    	List<Author> authors = new ArrayList<>();
        PreparedStatement preparedStatement = null;

        try {
        	preparedStatement = connectAndPrepareStatement(SQL_SELECT_NOT_EXPIRED_AUTHORS);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                authors.add(getAuthorFromResultSet(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DaoException("Can't load not expired authors.", e);
        }
        finally {
        	closeStatementAndDisconnect(preparedStatement);
        }

        return authors;
    }

    /**
     * Creates an Author instance and sets its fields with values from query result.
     * @param resultSet The returned result set.
     * @return Created Author instance.
     */
    private Author getAuthorFromResultSet(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong(1);
        String name = resultSet.getString(2);
        Date expired = resultSet.getTimestamp(3);

        return new Author(id, name, expired);
    }
}