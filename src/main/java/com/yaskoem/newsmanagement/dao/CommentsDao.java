package com.yaskoem.newsmanagement.dao;

import com.yaskoem.newsmanagement.domain.Comment;

import java.util.List;

/**
 * CommentsDao is used to get/put comments-related data from or into data source.
 * @author Alena Yasko
 */
public interface CommentsDao {

    /**
     * Adds a new record to authors data source.
     * <tt>creationDate</tt> field is automatically set to current date.
     *
     * @param comment an object that represents a new record to be added.
     * @return id of the inserted record.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    Long add(Comment comment);

    /**
     * Finds a list of comments related to news with the specified id.
     *
     * @param newsId id of the news related to the requested comment.
     * @return loaded list of comments. If no comments where found an empty list is returned.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    List<Comment> loadByNewsId(long newsId);

	/**
     * Deletes comment with the specified id from data source.
     * @param commentId id of a comment to be deleted.
     * @throws com.yaskoem.newsmanagement.exception.DaoException
     */
    void delete(long commentId);

	/**
     * Deletes all comments related to a news record with the specified id.
     * @param newsId id of the news which comments are to be deleted.
     */
    void deleteAllByNewsId(long newsId);
}