package com.yaskoem.newsmanagement.service;

import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.Tag;

/**
 * AdminFacadeService is used to call admin-specific service methods.
 * Calls methods of DAO layer objects.
 *
 * @author Alena Yasko
 */
public interface AdminFacadeService {

	/**
     * Gets an Author object with the specified id from data source.
     *
     * @param authorId
     * @return found author or <tt>null</tt> if no author with id equal to <tt>authorId</tt> was found.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    Author getAuthor(long authorId);

    /**
     * Gets a Tag object with the specified id from data source.
     *
     * @param tagId
     * @return found author or <tt>null</tt> if no tag with id equal to <tt>tagId</tt> was found.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    Tag getTag(long tagId);

    /**
     * Adds new news record and news-related author and tags records to data source.
     *
     * @param newsTO an object which fields represent new records to be inserted.
     * @return generated <tt>id</tt> of the new record of news.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    long addNews(NewsTO newsTO);

    /**
     * Updates news record and news-related author and tags records in data source.
     *
     * @param newsTO an object which fields represent new field values for update.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void updateNews(NewsTO newsTO);

    /**
     * Deletes news record, news' comments and relation records between author and tags and news
     * from data source.
     *
     * @param newsId id of the news to be deleted.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void deleteNews(long newsId);

    /**
     * Deletes comment record from data source.
     *
     * @param commentId id of the comment record to be deleted.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void deleteComment(long commentId);

    /**
     * Adds new tag record to data source.
     *
     * @param tag an object which fields represent new records to be inserted.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void addTag(Tag tag);

    /**
     * Updates tag record in data source.
     *
     * @param tag an object which fields represent new field values for update.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void updateTag(Tag tag);

    /**
     * Deletes tag record from data source.
     *
     * @param tagId id of the tag record to be deleted.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void deleteTag(Long tagId);

    /**
     * Adds new author record to data source.
     *
     * @param author an object which fields represent new records to be inserted.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void addAuthor(Author author);

    /**
     * Updates author record in data source.
     *
     * @param author an object which fields represent new field values for update.
     * @throws com.yaskoem.newsmanagement.exception.ServiceException
     */
    void updateAuthor(Author author);
}
