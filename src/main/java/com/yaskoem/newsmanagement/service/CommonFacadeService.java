package com.yaskoem.newsmanagement.service;

import java.util.List;

import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.domain.Tag;

/**
 * CommonFacadeService is used to call service methods common for client and admin.
 * Calls methods of DAO layer objects.
 *
 * @author Alena Yasko
 */
public interface CommonFacadeService {

	/**
	 * Gets <tt>maxCount</tt> news that meet the requirements of <tt>searchCriteria</tt> starting with
	 * <tt>firstNewsNumber</tt> results row number.
	 *
	 * @param searchCriteria
	 * @param firstNewsNumber start index of range in result list.
	 * @param maxCount maximum number of news to be retrieved.
	 * @return list of found news, or an empty list if no news found.
	 */
	List<NewsTO> getNewsList(SearchCriteria searchCriteria, int firstNewsNumber, int maxCount);

	/**
	 * Gets news TO with the specified id from data source.
	 *
	 * @param id
	 * @return found news and news-related data(comments, author, tags).
	 */
	NewsTO getNewsById(long id);

	/**
	 * Gets news TO of a previous neighbour of news with the specified id from data source.
	 *
	 * @param id
	 * @return found news and news-related data(comments, author, tags).
	 */
	NewsTO getPreviousNews(SearchCriteria searchCriteria, long id);

	/**
	 * Gets news TO of a next neighbour of news with the specified id from data source.
	 *
	 * @param id
	 * @return found news and news-related data(comments, author, tags).
	 */
	NewsTO getNextNews(SearchCriteria searchCriteria, long id);

	/**
	 * Adds new comment record to data source.
	 *
	 * @param comment an object which fields represent new records to be inserted.
	 * @throws com.yaskoem.newsmanagement.exception.ServiceException
	 */
	void addComment(Comment comment);

	/**
	 * Counts news that meet the requirements of <tt>searchCriteria</tt> parameter.
	 *
	 * @param searchCriteria
	 * @return number of news.
	 */
	long countNews(SearchCriteria searchCriteria);

	/**
	 * Gets all tags.
	 *
	 * @return list of found tags, or an empty list if no tags found.
	 */
	List<Tag> getTagsList();

	/**
	 * Gets all authors which <tt>expired</tt> field is <tt>null</tt>.
	 *
	 * @return list of found authors, or an empty list if no authors found.
	 */
	List<Author> getNotExpiredAuthors();
}