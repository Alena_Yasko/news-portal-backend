package com.yaskoem.newsmanagement.service.impl;

import com.yaskoem.newsmanagement.dao.AuthorsDao;
import com.yaskoem.newsmanagement.dao.CommentsDao;
import com.yaskoem.newsmanagement.dao.NewsDao;
import com.yaskoem.newsmanagement.dao.TagsDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.logging.Loggable;
import com.yaskoem.newsmanagement.service.AdminFacadeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * AdminFacadeServiceImpl calls methods of oracle implementation of DAO layer. 
 * @author Alena_Yasko
 */
public class AdminFacadeServiceImpl implements AdminFacadeService {

	/** news DAO */
    private NewsDao newsDao;

    /** comments DAO */
    private CommentsDao commentsDao;

    /** tags DAO */
    private TagsDao tagsDao;

    /** authors DAO */
    private AuthorsDao authorsDao;

    /**
     * Constructor.
     * @param newsDao News DAO.
     * @param commentsDao Comments DAO.
     * @param tagsDao Tags DAO.
     * @param authorsDao Authors DAO.
     */
    @Autowired
    public AdminFacadeServiceImpl(NewsDao newsDao, CommentsDao commentsDao, TagsDao tagsDao, AuthorsDao authorsDao) {
        this.newsDao = newsDao;
        this.commentsDao = commentsDao;
        this.tagsDao = tagsDao;
        this.authorsDao = authorsDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public Author getAuthor(long authorID) {
        return authorsDao.load(authorID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public Tag getTag(long tagId) {
        return tagsDao.load(tagId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public long addNews(NewsTO newsTO) {
        News news = newsTO.getNews();
        long newsId = newsDao.add(news);

        long authorId = newsTO.getAuthor().getId();
        authorsDao.bindToNews(newsId, authorId);

        bindTagsToNews(newsId, getTagIds(newsTO));

        return newsId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public void updateNews(NewsTO newsTO) {
        News news = newsTO.getNews();
        newsDao.update(news);

        long authorId = newsTO.getAuthor().getId();
        authorsDao.unbindFromNews(news.getId());
        authorsDao.bindToNews(news.getId(), authorId);

        bindTagsToNews(news.getId(), getTagIds(newsTO));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public void deleteNews(long newsId) {
        authorsDao.unbindFromNews(newsId);
        tagsDao.unbindAllFromNews(newsId);
        commentsDao.deleteAllByNewsId(newsId);
        newsDao.delete(newsId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public void deleteComment(long commentId) {
        commentsDao.delete(commentId);
    }

    @Override
    @Loggable
    public void addTag(Tag tag) {
        tagsDao.add(tag);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public void updateTag(Tag tag) {
        tagsDao.update(tag);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public void deleteTag(Long tagId) {
        tagsDao.unbindFromAllNews(tagId);
        tagsDao.delete(tagId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public void addAuthor(Author author) {
        authorsDao.add(author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Loggable
    public void updateAuthor(Author author) {
        authorsDao.update(author);
    }

    /**
     * Gets the list of tags' IDs from the given news transfer object.
     * @param newsTO The news transfer object.
     * @return The list of tags' IDs.
     */
    private List<Long> getTagIds(NewsTO newsTO) {
        return newsTO.getTags().stream()
            .map(Tag::getId)
            .collect(Collectors.toList());
    }

    /**
     * Updates tags' IDs list in news transfer object with the specified news ID.
     * @param newsId The news item ID.
     * @param The list of tags' IDs to bind to the news item.
     */
    private void bindTagsToNews(long newsId, List<Long> tagsIds) {
        List<Tag> oldTags = tagsDao.loadByNewsId(newsId);

        for (Tag oldTag : oldTags) {
            if (tagsIds.contains(oldTag.getId())) {
                tagsIds.remove(oldTag.getId());
            }
            else {
                tagsDao.unbindFromNews(newsId, oldTag.getId());
            }
        }

        for (long tagId : tagsIds) {
            tagsDao.bindToNews(newsId, tagId);
        }
    }
}