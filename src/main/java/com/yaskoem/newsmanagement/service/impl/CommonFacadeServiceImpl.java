package com.yaskoem.newsmanagement.service.impl;

import com.yaskoem.newsmanagement.dao.AuthorsDao;
import com.yaskoem.newsmanagement.dao.CommentsDao;
import com.yaskoem.newsmanagement.dao.NewsDao;
import com.yaskoem.newsmanagement.dao.TagsDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.logging.Loggable;
import com.yaskoem.newsmanagement.service.CommonFacadeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * CommonFacadeServiceImpl calls methods of oracle implementation of DAO layer.
 * @author Alena_Yasko
 */
public class CommonFacadeServiceImpl implements CommonFacadeService {

	/** news DAO */
    private NewsDao newsDao;

    /** comments DAO */
    private CommentsDao commentsDao;

    /** tags DAO */
    private TagsDao tagsDao;

    /** authors DAO */
    private AuthorsDao authorsDao;

    /**
     * Constructor.
     * @param newsDao News DAO.
     * @param commentsDao Comments DAO.
     * @param tagsDao Tags DAO.
     * @param authorsDao Authors DAO.
     */
    @Autowired
    public CommonFacadeServiceImpl(NewsDao newsDao, CommentsDao commentsDao, TagsDao tagsDao, AuthorsDao authorsDao) {
        this.newsDao = newsDao;
        this.commentsDao = commentsDao;
        this.tagsDao = tagsDao;
        this.authorsDao = authorsDao;
    }

    /**
     * Fetches news from data source by the specified search criteria 
     * and creates news transfer objects for the list, retrieving authors, comments, 
     * tags for each news item from data source.
     * The method is executed in a transaction.
     * 
     * @param searchCriteria The criteria for news selection.
     * @param firstNewsNumber Index of the first news item to fetch.
     * @param maxCount Maximum news items number to fetch.
     * @return The list of news transfer objects.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public List<NewsTO> getNewsList(SearchCriteria searchCriteria, int firstNewsNumber, int maxCount) {
        List<News> newsList = newsDao.loadList(searchCriteria, firstNewsNumber, maxCount);

        return newsList.stream()
            .map(this::getNewsTO)
            .collect(Collectors.toList());
    }

    /**
     * Returns news transfer object with the specified news ID.
     * The news item, its author, tags, comments are retrived from data source in a transaction.
     * @param newsId The ID of the news to fetch.
     * @return News transfer object.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public NewsTO getNewsById(long newsId) {
        News news = newsDao.load(newsId);
        return news != null ? getNewsTO(news) : null;
    }

    /**
     * Returns news transfer object for a news item, preceding to one with the specified ID in
     * the news list for the specified search criteria.
     * The news item, its author, tags, comments are retrived from data source in a transaction.
     * 
     * @param searchCriteria Searching criteria.
     * @param newsId The ID of the news to fetch.
     * @return News transfer object.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public NewsTO getPreviousNews(SearchCriteria searchCriteria, long newsId) {
        News news = newsDao.loadPrevious(searchCriteria, newsId);
        return news != null ? getNewsTO(news) : null;
    }

    /**
     * Returns news transfer object for a news item, succeeding to one with the specified ID in
     * the news list for the specified search criteria.
     * The news item, its author, tags, comments are retrived from data source in a transaction.
     * 
     * @param searchCriteria Searching criteria.
     * @param newsId The ID of the news to fetch.
     * @return News transfer object.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Loggable
    public NewsTO getNextNews(SearchCriteria searchCriteria, long newsId) {
        News news = newsDao.loadNext(searchCriteria, newsId);
        return news != null ? getNewsTO(news) : null;
    }

    /**
     * Stores new comment in data source.
     * @param comment The new comment to store.
     */
    @Override
    @Loggable
    public void addComment(Comment comment) {
        commentsDao.add(comment);
    }

    /**
     * Counts all news fetched by search criteria.
     * @param searchCriteria The search criteria.
     * @return The number of news.
     */
    @Override
    @Loggable
    public long countNews(SearchCriteria searchCriteria) {
        return newsDao.count(searchCriteria);
    }

    /**
     * Fetches all tags from data source.
     * @return Tags list.
     */
    @Override
    @Loggable
    public List<Tag> getTagsList() {
        return tagsDao.loadAll();
    }

    /**
     * Fetches non-expired authors from data source.
     * @return Non-expired authors list.
     */
    @Override
    @Loggable
    public List<Author> getNotExpiredAuthors() {
        return authorsDao.loadNotExpired();
    }

    /**
     * Fetches author, comments and tags for the specified new item from data source and
     * creates news transfer object.
     * @param news The news item.
     * @return News transfer object.
     */
    private NewsTO getNewsTO(News news) {
        Author author = authorsDao.loadByNewsId(news.getId());
        List<Tag> tags = tagsDao.loadByNewsId(news.getId());
        List<Comment> comments = commentsDao.loadByNewsId(news.getId());

        return new NewsTO(news, author, tags, comments);
    }
}