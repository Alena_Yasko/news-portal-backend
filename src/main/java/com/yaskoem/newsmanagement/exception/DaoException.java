package com.yaskoem.newsmanagement.exception;

/**
 * Exception in DAO layer.
 * @author Alena_Yasko
 */
public class DaoException extends RuntimeException {

	/**
	 * Constructor.
	 * @param message Error message.
	 */
    public DaoException(String message) {
        super(message);
    }

	/**
	 * Constructor.
	 * @param message Error message.
	 * @param cause Class causing the exception.
	 */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

	/**
	 * Constructor.
	 * @param cause Class causing the exception.
	 */
    public DaoException(Throwable cause) {
        super(cause);
    }
}