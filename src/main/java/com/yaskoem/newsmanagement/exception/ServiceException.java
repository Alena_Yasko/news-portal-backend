package com.yaskoem.newsmanagement.exception;

/**
 * Exception in service layer.
 * @author Alena_Yasko
 */
public class ServiceException extends RuntimeException {

	/**
	 * Constructor.
	 * @param cause Class causing the exception.
	 */
    public ServiceException(Throwable cause) {
        super(cause);
    }

	/**
	 * Constructor.
	 * @param message Error message.
	 * @param cause Class causing the exception.
	 */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

	/**
	 * Constructor.
	 * @param message Error message.
	 */
    public ServiceException(String message) {
        super(message);
    }
}
