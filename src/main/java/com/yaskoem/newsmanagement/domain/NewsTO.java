package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Transfer object class for news. Contains news item, its comments, tags and author.
 * @author Alena_Yasko
 */
public class NewsTO implements Serializable {

    private News news;

    private Author author;

    private List<Comment> comments = new ArrayList<>();

    private List<Tag> tags = new ArrayList<>();

    /**
     * Default constructor.
     */
    public NewsTO() {
    }

    /**
     * Constructor.
     * @param news News item.
     * @param author The news item's author.
     */
    public NewsTO(News news, Author author) {
        this.news = news;
        this.author = author;
    }

    /**
     * Constructor.
     * @param news News item.
     * @param author The news item's author.
     * @param tags Tags list for the news item.
     */
    public NewsTO(News news, Author author, List<Tag> tags) {
        this(news, author);
        this.tags = tags;
    }

    /**
     * Constructor.
     * @param news News item.
     * @param author The news item's author.
     * @param comments Comments list for the news item.
     */
    public NewsTO(News news, Author author, List<Tag> tags, List<Comment> comments) {
        this(news, author, tags);
        this.comments = comments;
    }

    /**
     * Gets news item.
     * @return News item.
     */
    public News getNews() {
        return news;
    }

    /**
     * Sets news item.
     * @param news News item.
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * Gets author.
     * @return Author.
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets author.
     * @param author Author.
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Gets comments list for the news item.
     * @return Comments list.
     */
    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * Gets tags list for the news item.
     * @return Tags list.
     */
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        NewsTO newsTO = (NewsTO) obj;

        return news != null ? news.equals(newsTO.news) : newsTO.news == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return news != null ? news.hashCode() : 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "NewsTO{" +
            "news=" + news +
            ", author=" + author +
            ", comments=" + Arrays.toString(comments.toArray()) +
            ", tags=" + tags +
            '}';
    }
}