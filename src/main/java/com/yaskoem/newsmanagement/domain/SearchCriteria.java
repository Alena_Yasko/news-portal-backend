package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Searching criteria class.
 * @author Alena_Yasko
 */
public class SearchCriteria implements Serializable {
	
	/** Author ID */
	private Long authorId;
	
	/** List of tags' IDs */
	private List<Long> tagsIds;

	/**
	 * Default constructor.
	 */
	public SearchCriteria() {
	}
	
	/**
	 * Constructor.
	 * @param authorId Author ID.
	 */
	public SearchCriteria(long authorId) {
		this.authorId = authorId;
	}
	
	/**
	 * Constructor.
	 * @param tagsIds List of tags' IDs.
	 */	
	public SearchCriteria(List<Long> tagsIds) {
		this.tagsIds = tagsIds;
	}

	/**
	 * Constructor.
	 * @param authorId Author ID.
	 * @param tagsIds List of tags' IDs.
	 */
	public SearchCriteria(long authorId, List<Long> tagsIds) {
		this.authorId = authorId;
		this.tagsIds = tagsIds;
	}
	
	/** 
	 * Gets author ID.
	 * @return Author ID.
	 */
	public Long getAuthorId() {
		return authorId;
	}
	
	/** 
	 * Sets author ID.
	 * @param authorId Author ID.
	 */
	public void setAuthorId(Long authorId) {
		if (authorId > 0) {
			this.authorId = authorId;
		}
	}

	/** 
	 * Gets the list of tags' IDs.
	 * @return List of tags' IDs.
	 */
	public List<Long> getTagsIds() {
		return tagsIds;
	}

	/**
	 * Sets the list of tags' IDs.
	 * @param tagsIds List of tags' IDs.
	 */
	public void setTagsIds(List<Long> tagsIds) {
		this.tagsIds = tagsIds;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public String toString() {
		return "SearchCriteria{" +
			"authorId=" + (authorId != null ? authorId : "null") +
			", tagsIds=" + (tagsIds != null ? Arrays.toString(tagsIds.toArray()) : "null") +
			'}';
	}
}