package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Author.
 * @author Alena_Yasko
 */
public class Author implements Serializable {

	/** ID */
    private Long id;

    /** First and last name */
    private String name;

    /** Expiration date */
    private Date expired;

    /**
     * Default constructor.
     */
    public Author() {
    }
    
    /**
     * Constructor.
     * @param name First and last name.
     */
    public Author(String name) {
        this.name = name;
    }

    /**
     * Constructor.
     * @param id ID.
     * @param name First and last name.
     */
    public Author(Long id, String name) {
        this(name);
        this.id = id;
    }
    
    /**
     * Constructor.
     * @param id ID.
     * @param name First and last name.
     * @param expired Expiration date. 
     */
    public Author(Long id, String name, Date expired) {
        this(id, name);
        this.expired = expired;
    }

    /**
     * Gets ID.
     * @return ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets ID.
     * @param id ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets first and last name.
     * @return First and last name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets first and last name.
     * @param name First and last name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets expiration date.
     * @return Expiration date.
     */
    public Date getExpired() {
        return expired;
    }

    /**
     * Sets expiration date.
     * @param expired Expiration date.
     */
    public void setExpired(Date expired) {
        this.expired = expired;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        
    	if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Author author = (Author) obj;

        return id != null ? id.equals(author.id) : author.id == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Author{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", expired=" + expired +
            '}';
    }
}