package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.util.Assert;

/**
 * News item class.
 * @author Alena_Yasko
 */
public class News implements Serializable {

	/** ID */
    private Long id;

    /** Title */
    private String title;

    /** Short description */
    private String shortText;

    /** Full text */
    private String fullText;

    /** News item creation date */
    private Date creationDate;

    /** News item modification date */
    private Date modificationDate;

    /**
     * Default constructor.
     */
    public News() {
    }
    
    /**
     * Constructor.
     * @param title Title.
     * @param shortText Short description.
     * @param fullText Full text.
     */
    public News(String title, String shortText, String fullText) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
    }
    
    /**
     * Constructor.
     * @param id ID.
     * @param title Title.
     * @param shortText Short description.
     * @param fullText Full text.
     */
    public News(Long id, String title, String shortText, String fullText) {
        this(title, shortText, fullText);

        Assert.notNull(id, "id must not be null!");

    	this.id = id;
    }

    /**
     * Constructor.
     * @param id ID.
     * @param title Title.
     * @param shortText Short description.
     * @param fullText Full text.
     * @param creationDate News item creation date.
     * @param modificationDate News item modification date.
     */
    public News(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
        this(id, title, shortText, fullText);

        Assert.notNull(creationDate, "creationDate must not be null!");
        Assert.notNull(modificationDate, "modificationDate must not be null!");

    	this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }
    
    /**
     * Sets ID.
     * @param id News ID.
     */
    public void setId(long id) {
    	this.id = id;
    }

    /**
     * Gets ID.
     * @return News ID.
     */
    public Long getId() {
    	return id;
    }
    
    /**
     * Gets title.
     * @return Title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     * @param title Title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets short description.
     * @return Short description.
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * Sets short description.
     * @param shortText Short description.
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * Gets short description.
     * @return Short description.
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * Gets full text.
     * @param fullText Full text.
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /** 
     * Gets news item creation date.
     * @return Creation date.
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /** 
     * Sets news item creation date.
     * @param creationDate Creation date.
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /** 
     * Gets news item modification date.
     * @return Modification date.
     */
    public Date getModificationDate() {
        return modificationDate;
    }

    /** 
     * Sets news item modification date.
     * @param modificationDate Modification date.
     */
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        News news = (News) obj;

        return id != null ? id.equals(news.id) : news.id == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "News{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", shortText='" + shortText + '\'' +
            ", fullText='" + fullText + '\'' +
            ", creationDate=" + creationDate +
            ", modificationDate=" + modificationDate +
            '}';
    }
}