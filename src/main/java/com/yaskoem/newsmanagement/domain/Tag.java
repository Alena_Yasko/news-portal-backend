package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;

/**
 * Tag.
 * @author Alena Yasko
 */
public class Tag implements Serializable {

	/** ID */
    private Long id;

    /** Tag name */
    private String name;

    /** 
     * Default constructor.
     */
    public Tag() {
    }
    
    /** 
     * Constructor.
     * @param name Tag name. 
     */
    public Tag(String name) {
        this.name = name;
    }

    /** 
     * Constructor.
     * @param id ID.
     * @param name Tag name. 
     */
    public Tag(Long id, String name) {
        this(name);
        this.id = id;
    }

    /**
     * Gets tag ID.
     * @return Tag ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets tag ID.
     * @param id Tag ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets tag name.
     * @return Tag name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets tag name.
     * @param name Tag name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Tag tag = (Tag) obj;

        return id != null ? id.equals(tag.id) : tag.id == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Tag{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}