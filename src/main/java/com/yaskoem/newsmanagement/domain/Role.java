package com.yaskoem.newsmanagement.domain;

/**
 * User role.
 * @author Alena_Yasko
 */
public interface Role {

	/** Admin */
    String ADMIN = "ROLE_ADMIN";

    /** Regular client */
    String USER = "ROLE_USER";
}
