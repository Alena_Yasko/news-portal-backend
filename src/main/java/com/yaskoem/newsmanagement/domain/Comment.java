package com.yaskoem.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Comment.
 * @author Alena_Yasko
 */
public class Comment implements Serializable {

	/** Comment ID */
    private Long id;

    /** ID of the commented news. */
    private Long newsId;

    /** Comment text */
    private String text;

    /** Creation date */
    private Date creationDate;

    /**
     * Default constructor.
     */
    public Comment() {
    }

    
    /**
     * Constructor.
     * @param text Comment text.
     */
    public Comment(String text) {
        this.text = text;
    }

    /**
     * Constructor.
     * @param newsId ID of the commented news.
     * @param text Comment text.
     */
    public Comment(Long newsId, String text) {
        this(text);
        this.newsId = newsId;
    }
    
    /**
     * Constructor.
     * @param id Comment ID.
     * @param newsId ID of the commented news.
     * @param text Comment text.
     * @param creationDate Comment creation date.
     */
    public Comment(Long id, Long newsId, String text, Date creationDate) {
    	this(newsId, text);
    	this.id = id;
    	this.creationDate = creationDate;
    }

    /**
     * Gets comment ID.
     * @return Comment ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets comment ID.
     * @param id Comment ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets comment text.
     * @return Comment text.
     */
    public String getText() {
        return text;
    }

    /**
     * Sets comment text.
     * @param text Comment text.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets news item ID.
     * @return News item ID.
     */
    public Long getNewsId() {
        return newsId;
    }
    
    /**
     * Sets news item ID.
     * @param newsId News item ID.
     */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }
    
    /**
     * Gets comment creation date.
     * @return Comment creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Sets comment creation date.
     * @param creationDate Comment creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Comment comment = (Comment) obj;

        return id != null ? id.equals(comment.id) : comment.id == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Comment{" +
            "id=" + id +
            ", newsId=" + newsId +
            ", text='" + text + '\'' +
            ", creationDate=" + creationDate +
            '}';
    }
}