package com.yaskoem.newsmanagement.logging;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spring AOP logger.
 * @see Loggable
 *
 * @author Alena Yasko
 */
@Aspect
public class LoggingAspect {

	/** The logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

	/**
     * Logs the name and parameter value of called methods that are marked with <tt>@Loggable</tt>.
     *
     * @param joinPoint The joint point.
     * @param loggable Loggable method.
     * @return -
     * @throws Throwable In case of an error.
     */
    @Around("@annotation(loggable)")
    public Object invoke(ProceedingJoinPoint joinPoint, Loggable loggable) throws Throwable {
        LOGGER.debug(joinPoint.getSignature().getName() + Arrays.toString(joinPoint.getArgs()));
        return joinPoint.proceed();
    }
}