package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.dao.oracle.AuthorsOracleDao;
import com.yaskoem.newsmanagement.domain.Author;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;
import java.util.Locale;

/**
 * DBUunit test class for {@link AuthorsOracleDao}
 * @author Alena_Yasko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-authors-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-authors-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorsDaoTest {

	/** Set test DB locale */
    static {
        Locale.setDefault(Locale.US);
    }

    /** Tested class */
    @Autowired
    private AuthorsOracleDao authorsOracleDao;

    /**
     * Tests add(Author).
     */
    @Test
    public void addTest() {
        Author author = new Author("Patrick Swayze");

        long authorId = authorsOracleDao.add(author);
        author.setId(authorId);

        Author insertedAuthor = authorsOracleDao.load(authorId);
        Assert.assertEquals(author, insertedAuthor);
    }

    /**
     * Tests bindToNews(long, long).
     */
    @Test
    public void bindToNewsTest() {
        long newsId = 2L;
        long authorId = 2L;

        Author oldNewsAuthor = authorsOracleDao.loadByNewsId(newsId);
        Assert.assertNull(oldNewsAuthor);
        authorsOracleDao.bindToNews(newsId, authorId);
        Author newNewsAuthor = authorsOracleDao.loadByNewsId(newsId);

        Assert.assertTrue(authorId == newNewsAuthor.getId());
    }

    /**
     * Tests unbindToNews(long).
     */
    @Test
    public void unbindFromNewsTest() {
        long newsId = 1L;

        authorsOracleDao.unbindFromNews(newsId);
        Author newNewsAuthor = authorsOracleDao.loadByNewsId(newsId);

        Assert.assertNull(newNewsAuthor);
    }

    /**
     * Tests load(long).
     */
    @Test
    public void loadTest() {
        long authorId = 2L;

        Author author = new Author(authorId, "James Smith", null);
        Author loadedAuthor = authorsOracleDao.load(authorId);

        Assert.assertEquals(author, loadedAuthor);
    }

    /**
     * Tests loadByNewsId(long).
     */
    @Test
    public void loadByNewsIdTest() {
        long newsId = 3L;
        Author author = new Author(3L, "Robert Brown", null);

        Author loadedAuthor = authorsOracleDao.loadByNewsId(newsId);

        Assert.assertEquals(author, loadedAuthor);
    }

    /**
     * Tests loadNotExpired().
     */
    @Test
    public void loadNotExpiredTest() {
        int notExpiredCount = 2;
        List<Author> notExpiredAuthors = authorsOracleDao.loadNotExpired();

        Assert.assertTrue(notExpiredCount == notExpiredAuthors.size());

        for (Author author : notExpiredAuthors) {
            Assert.assertNull(author.getExpired());
        }
    }
}