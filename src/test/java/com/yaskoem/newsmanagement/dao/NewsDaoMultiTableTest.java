package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.dao.oracle.NewsOracleDao;
import com.yaskoem.newsmanagement.domain.News;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * DBUnit test class for {@link NewsOracleDao} multi-table operations.
 * @author Alena_Yasko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-news-multitable-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-news-multitable-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDaoMultiTableTest {

	/* set test database locale */
    static {
        Locale.setDefault(Locale.US);
    }

    /** Tested class */
    @Autowired
    private NewsOracleDao newsOracleDao;

    /**
     * Tests count().
     * Search criteria uses multiple tables to fetch requested news.
     */
    @Test
    public void countTest() {
        SearchCriteria searchCriteria = new SearchCriteria();
        long newsCount = newsOracleDao.count(searchCriteria);
        Assert.assertTrue(newsCount == 3);
    }

    /**
     * Tests loadList(SearchCriteria, int, int).
     */
    @Test
    public void loadListTest() {
        int firstNewsNumber = 1;
        int maxCount = 100;
        News news1 = newsOracleDao.load(1L);
        News news2 = newsOracleDao.load(2L);
        News news3 = newsOracleDao.load(3L);

        List<Long> tagsIds = new ArrayList<Long>() {{
            add(1L);
            add(2L);
        }};

        SearchCriteria criteria1 = new SearchCriteria(1L, tagsIds);
        List<News> newsList = newsOracleDao.loadList(criteria1, firstNewsNumber, maxCount);
        Assert.assertTrue(newsList.size() == 2);

        Assert.assertEquals(news2, newsList.get(0));

        SearchCriteria criteria2 = new SearchCriteria(tagsIds);

        newsList = newsOracleDao.loadList(criteria2, firstNewsNumber, maxCount);
        Assert.assertTrue(newsList.size() == 2);
        Assert.assertEquals(news2, newsList.get(0));
        Assert.assertEquals(news1, newsList.get(1));

        SearchCriteria criteria3 = new SearchCriteria(3L, tagsIds);

        newsList = newsOracleDao.loadList(criteria3, firstNewsNumber, maxCount);
        Assert.assertTrue(newsList.isEmpty());

        SearchCriteria criteria4 = new SearchCriteria(3L);
        newsList = newsOracleDao.loadList(criteria4, firstNewsNumber, maxCount);
        Assert.assertTrue(newsList.size() == 1);
        Assert.assertEquals(news3, newsList.get(0));

        SearchCriteria criteria5 = new SearchCriteria();
        firstNewsNumber = 2;

        newsList = newsOracleDao.loadList(criteria5, firstNewsNumber, maxCount);
        Assert.assertTrue(newsList.size() == 2);
        Assert.assertTrue(newsList.get(0).equals(news1));
        Assert.assertTrue(newsList.get(1).equals(news3));
        Assert.assertFalse(newsList.contains(news2));
    }

    /**
     * Tests loadPrevious(long).
     */
    @Test
    public void loadPreviousAndNextTest() {
        long prevNewsId = 2L;
        long newsId = 1L;

        SearchCriteria searchCriteria = new SearchCriteria();

        News loadedNews = newsOracleDao.loadPrevious(searchCriteria, newsId);
        Assert.assertTrue(prevNewsId == loadedNews.getId());
    }
    
    /**
     * Tests loadNext(long).
     */
    @Test
    public void loadNextTest() {
        long newsId = 1L;
        long nextNewsId = 3L;

        SearchCriteria searchCriteria = new SearchCriteria();

        News loadedNews = newsOracleDao.loadNext(searchCriteria, newsId);
        Assert.assertTrue(nextNewsId == loadedNews.getId());
    }
}