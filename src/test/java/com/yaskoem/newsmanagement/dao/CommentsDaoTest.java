package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.dao.oracle.CommentsOracleDao;
import com.yaskoem.newsmanagement.domain.Comment;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Locale;

/**
 * DBUunit test class for {@link CommentsOracleDao}
 * @author Alena_Yasko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-comments-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-comments-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class CommentsDaoTest {

	/** Set test DB locale */
    static {
        Locale.setDefault(Locale.US);
    }

    /** Tested class */
    @Autowired
    private CommentsOracleDao commentsOracleDao;

    /**
     * Tests add(Comment).
     */
    @Test
    public void addTest() {
        long newsId = 1L;
        Comment comment = new Comment(newsId, "DBUnit comment text");

        comment.setId(commentsOracleDao.add(comment));

        List<Comment> newComments = commentsOracleDao.loadByNewsId(newsId);

        Assert.assertTrue(newComments.contains(comment));
    }

    /**
     * Tests loadByNewsId(long).
     * @throws ParseException In case of a wrong test date-time string for comment creation date.
     */
    @Test
    public void loadByNewsIdTest() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date date1 = formatter.parse("2017-01-17 03:41:25.130");
        Date date2 = formatter.parse("2017-01-17 03:42:25.130");

        long newsId = 2L;

        List<Comment> expectedComments = new ArrayList<Comment>() {{
            add(new Comment(2L, newsId, "Comment 2", date1));
            add(new Comment(3L, newsId, "Comment 3", date2));
        }};

        List<Comment> loadedComments = commentsOracleDao.loadByNewsId(newsId);
        Assert.assertTrue(expectedComments.size() == loadedComments.size());

        for (Comment comment : expectedComments) {
            Assert.assertTrue(loadedComments.contains(comment));
        }
    }

    /**
     * Tests delete(long).
     */
    @Test
    public void deleteTest() {
        long newsId = 2L;

        List<Comment> comments = commentsOracleDao.loadByNewsId(newsId);
        Comment commentToDelete = comments.get(0);
        commentsOracleDao.delete(commentToDelete.getId());

        List<Comment> newComments = commentsOracleDao.loadByNewsId(newsId);
        Assert.assertFalse(newComments.contains(commentToDelete));
    }

    /**
     * Tests deleteAllByNewsId(long).
     */
    @Test
    public void deleteAllByNewsIdTest() {
        long newsId = 2L;

        List<Comment> comments = commentsOracleDao.loadByNewsId(newsId);
        Assert.assertFalse(comments.isEmpty());

        commentsOracleDao.deleteAllByNewsId(newsId);
        comments = commentsOracleDao.loadByNewsId(newsId);
        Assert.assertTrue(comments.isEmpty());
    }
}
