package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.dao.oracle.TagsOracleDao;
import com.yaskoem.newsmanagement.domain.Tag;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;
import java.util.Locale;

/**
 * DBUnit test class for {@link TagsOracleDao}.
 * @author Alena_Yasko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-tags-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-tags-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TagsDaoTest {

	/** set test database locale */
    static {
        Locale.setDefault(Locale.US);
    }

    /** Tested class */
    @Autowired
    private TagsOracleDao tagsOracleDao;

    /**
     * Tests add(Tag).
     */
    @Test
    public void addTest() {
        Tag newTag = new Tag("literature");

        long newTagId = tagsOracleDao.add(newTag);
        newTag.setId(newTagId);

        List<Tag> newAllTags = tagsOracleDao.loadAll();

        Assert.assertTrue(newAllTags.contains(newTag));
    }

    /**
     * Tests bindToNews(long, long).
     */
    @Test
    public void bindToNewsTest() {
        long newsId = 1L;
        Tag tagToBind = new Tag(3L, "nature");

        List<Tag> oldTagsList = tagsOracleDao.loadByNewsId(newsId);
        for (Tag tag : oldTagsList) {
            Assert.assertFalse(tag.getId().equals(tagToBind.getId()));
        }

        tagsOracleDao.bindToNews(newsId, tagToBind.getId());
        List<Tag> newTagsList = tagsOracleDao.loadByNewsId(newsId);
        Assert.assertTrue(newTagsList.contains(tagToBind));
    }

    /**
     * Tests load(long).
     */
    @Test
    public void loadTest() {
    	
        Tag tag = new Tag(3L, "nature");
        Tag loadedTag = tagsOracleDao.load(tag.getId());
        Assert.assertEquals(tag, loadedTag);
    }

    /**
     * Tests loadByNewsId(long).
     */
    @Test
    public void loadByNewsIdTest() {
        long newsId = 1L;
        Tag tag1 = new Tag(1L, "music");
        Tag tag2 = new Tag(2L, "science");

        List<Tag> newsTags = tagsOracleDao.loadByNewsId(newsId);
        Assert.assertTrue(newsTags.size() == 2);
        Assert.assertTrue(newsTags.contains(tag1));
        Assert.assertTrue(newsTags.contains(tag2));
    }

    /**
     * Tests loadAll().
     */
    @Test
    public void loadAllTest() {
        Tag tag1 = new Tag(1L, "music");
        Tag tag2 = new Tag(2L, "science");
        Tag tag3 = new Tag(3L, "nature");

        List<Tag> allTags = tagsOracleDao.loadAll();

        Assert.assertTrue(allTags.size() == 3);
        Assert.assertTrue(allTags.contains(tag1));
        Assert.assertTrue(allTags.contains(tag2));
        Assert.assertTrue(allTags.contains(tag3));
    }

    /**
     * Tests unbindFromNews(long, long).
     */
    @Test
    public void unbindFromNewsTest() {
        long newsId = 3L;
        long tagId = 3L;

        List<Tag> newsTags = tagsOracleDao.loadByNewsId(newsId);
        Assert.assertFalse(newsTags.isEmpty());

        tagsOracleDao.unbindFromNews(newsId, tagId);
        List<Tag> updatedNewsTags = tagsOracleDao.loadByNewsId(newsId);
        Assert.assertTrue(updatedNewsTags.isEmpty());
    }

    /**
     * Tests unbindFromAllNews(long).
     */
    @Test
    public void unbindFromAllNewsTest() {
        Tag tag = new Tag(1L, "music");

        long newsId1 = 1L;
        long newsId2 = 2L;

        List<Tag> news1Tags = tagsOracleDao.loadByNewsId(newsId1);
        Assert.assertTrue(news1Tags.contains(tag));

        List<Tag> news2Tags = tagsOracleDao.loadByNewsId(newsId2);
        Assert.assertTrue(news2Tags.contains(tag));

        tagsOracleDao.unbindFromAllNews(tag.getId());

        news1Tags = tagsOracleDao.loadByNewsId(newsId1);
        Assert.assertFalse(news1Tags.contains(tag));

        news2Tags = tagsOracleDao.loadByNewsId(newsId2);
        Assert.assertFalse(news2Tags.contains(tag));
    }

    /**
     * Tests unbindAllFromNews(long).
     */
    @Test
    public void unbindAllFromNewsTest() {
        long newsId = 1L;

        List<Tag> newsTags = tagsOracleDao.loadByNewsId(newsId);
        Assert.assertFalse(newsTags.isEmpty());

        tagsOracleDao.unbindAllFromNews(newsId);
        newsTags = tagsOracleDao.loadByNewsId(newsId);
        Assert.assertTrue(newsTags.isEmpty());
    }
}