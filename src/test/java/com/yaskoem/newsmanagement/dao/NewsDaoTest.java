package com.yaskoem.newsmanagement.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.yaskoem.newsmanagement.dao.oracle.NewsOracleDao;
import com.yaskoem.newsmanagement.domain.News;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * DBUnit test class for {@link NewsOracleDao}.
 * @author Alena_Yasko
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:dataset/test-news-dataset.xml")
@DatabaseTearDown(value = "classpath:dataset/test-news-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDaoTest {

	/** set test database locale */
    static {
        Locale.setDefault(Locale.US);
    }
    
    /** Tested class */
    @Autowired
    private NewsOracleDao newsOracleDao;

    /**
     * Tests add(News).
     */
    @Test
    public void addTest() {
        News news = new News("DBUnit news title", "Short text", "Full text");

        long newsId = newsOracleDao.add(news);
        news.setId(newsId);

        News loadedNews = newsOracleDao.load(newsId);

        Assert.assertEquals(news, loadedNews);
    }

    /**
     * Tests update(News).
     */
    @Test
    public void updateTest() {
        String newTitle = "New title";
        long newsId = 1L;

        News news = newsOracleDao.load(newsId);
        news.setTitle(newTitle);
        newsOracleDao.update(news);
        News loadedNews = newsOracleDao.load(newsId);

        Assert.assertEquals(newTitle, loadedNews.getTitle());
    }

    /**
     * Tests load(long).
     * @throws ParseException In case of a invalid test date-time strings.  
     */
    @Test
    public void loadTest() throws ParseException {
        DateFormat creationDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        DateFormat modificationDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date creationDate = creationDateFormatter.parse("2017-01-17 03:40:21.130");
        Date modificationDate = modificationDateFormatter.parse("2017-01-17");

        long newsId = 1L;

        News news = new News(newsId, "Test news 1", "Short text 1", "Full text 1", creationDate, modificationDate);
        News loadedNews = newsOracleDao.load(newsId);

        Assert.assertEquals(news, loadedNews);
    }

    /**
     * Tests delete(long).
     */
    @Test
    public void deleteTest() {
        long newsId = 1L;
        News news = newsOracleDao.load(newsId);
        Assert.assertNotNull(news);

        newsOracleDao.delete(newsId);
        news = newsOracleDao.load(newsId);
        Assert.assertNull(news);
    }
}