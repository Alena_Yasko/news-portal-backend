package com.yaskoem.newsmanagement.service;

import com.yaskoem.newsmanagement.dao.oracle.AuthorsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.CommentsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.NewsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.TagsOracleDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.Comment;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.SearchCriteria;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.service.impl.CommonFacadeServiceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * JUnit test class for {@link CommonFacadeServiceImpl}.
 * @author Alena_Yasko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
public class CommonFacadeServiceTest {

	/** News DAO mock */
    @Mock
    private NewsOracleDao newsOracleDaoMock;

    /** Authors DAO mock */
    @Mock
    private AuthorsOracleDao authorsOracleDaoMock;

    /** Tags DAO mock */
    @Mock
    private TagsOracleDao tagsOracleDaoMock;

    /** Comments DAO mock */
    @Mock
    private CommentsOracleDao commentsOracleDaoMock;

    /** Tested class */
    @InjectMocks
    private CommonFacadeServiceImpl commonFacadeServiceImpl;

    /**
     * Initialization.
     */
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests getNewsList(SearchCriteria, int, int).
     */
    @Test
    public void getNewsListTest() {
        SearchCriteria searchCriteria = new SearchCriteria();
        int first = 1;
        int maxCount = 10;
        
        Long newsId1 = 1L;
        Long newsId2 = 2L;
        
        News news1 = new News(newsId1, "DBUnit news title 1", "Short text 1", "Full text 1");
        News news2 = new News(newsId2, "DBUnit news title 2", "Short text 2", "Full text 2");
        List<News> newsList = Arrays.asList(news1, news2);
        List<Long> newsIds = Arrays.asList(newsId1, newsId2);
        
        Mockito.when(newsOracleDaoMock.loadList(searchCriteria, first, maxCount)).thenReturn(newsList);
        
        Author author1 = new Author(1L, "Patrick Swayze");
        Author author2 = null;
        
        Mockito.when(authorsOracleDaoMock.loadByNewsId(newsId1)).thenReturn(author1);
        Mockito.when(authorsOracleDaoMock.loadByNewsId(newsId2)).thenReturn(author2);
        
        List<Tag> tags1 = new ArrayList<>();
        List<Tag> tags2 = Arrays.asList(new Tag(1L, "nature"));
        
        Mockito.when(tagsOracleDaoMock.loadByNewsId(newsId1)).thenReturn(tags1);
        Mockito.when(tagsOracleDaoMock.loadByNewsId(newsId2)).thenReturn(tags2);

        Mockito.when(commentsOracleDaoMock.loadByNewsId(Mockito.anyLong())).thenReturn(new ArrayList<>());
                
        List<NewsTO> news = commonFacadeServiceImpl.getNewsList(searchCriteria, first, maxCount);
        
        Mockito.verify(newsOracleDaoMock, Mockito.times(1)).loadList(searchCriteria, first, maxCount);
        
        ArgumentCaptor<Long> newIdsCaptor = ArgumentCaptor.forClass(Long.class);
        
        Mockito.verify(authorsOracleDaoMock, Mockito.times(newsList.size())).loadByNewsId(newIdsCaptor.capture());
        for (Long capturedId : newIdsCaptor.getAllValues()) {
        	Assert.assertTrue(newsIds.contains(capturedId));
        }
        
        Mockito.verify(tagsOracleDaoMock, Mockito.times(newsList.size())).loadByNewsId(newIdsCaptor.capture());
        for (Long capturedId : newIdsCaptor.getAllValues()) {
        	Assert.assertTrue(newsIds.contains(capturedId));
        }
        
        Mockito.verify(commentsOracleDaoMock, Mockito.times(newsList.size())).loadByNewsId(newIdsCaptor.capture());
        for (Long capturedId : newIdsCaptor.getAllValues()) {
        	Assert.assertTrue(newsIds.contains(capturedId));
        }
        
        Assert.assertTrue(news.size() == newsList.size());
        for (NewsTO newsTO : news) {
        	if (newsTO.getNews().getId().equals(newsId1)) {
        		Assert.assertEquals(author1, newsTO.getAuthor());
        		Assert.assertEquals(tags1, newsTO.getTags());
        	}
        	else {
        		Assert.assertEquals(author2, newsTO.getAuthor());
        		Assert.assertEquals(tags2, newsTO.getTags());
        	}
        	Assert.assertTrue(newsTO.getComments().isEmpty());
        }
    }

    /**
     * Tests addComment(Comment).
     */
    @Test
    public void addCommentTest() {
        Comment comment = new Comment("Test comment");
        long newCommentId = 1L;

        Mockito.when(commentsOracleDaoMock.add(comment)).thenReturn(newCommentId);
        commonFacadeServiceImpl.addComment(comment);
        
        ArgumentCaptor<Comment> commentCaptor = ArgumentCaptor.forClass(Comment.class);
        Mockito.verify(commentsOracleDaoMock, Mockito.times(1)).add(commentCaptor.capture());
        
        Assert.assertEquals(comment, commentCaptor.getValue());
    }

    /**
     * Tests countNews(SearchCriteria).
     */
    @Test
    public void countNewsTest() {
        SearchCriteria searchCriteria = new SearchCriteria();
        long newsNumber = 3L;
        
        Mockito.when(newsOracleDaoMock.count(searchCriteria)).thenReturn(newsNumber);
        
        long actualNewsNumber = commonFacadeServiceImpl.countNews(searchCriteria);
        
        ArgumentCaptor<SearchCriteria> searchCriteriaCaptor = ArgumentCaptor.forClass(SearchCriteria.class);
        Mockito.verify(newsOracleDaoMock, Mockito.times(1)).count(searchCriteriaCaptor.capture());
        
        Assert.assertEquals(searchCriteria, searchCriteriaCaptor.getValue());
        Assert.assertEquals(newsNumber, actualNewsNumber);
    }

    /**
     * Tests getTagsList().
     */
    @Test
    public void getTagsListTest() {
    	List<Tag> tags = Arrays.asList(new Tag(1L, "nature"));
    	
    	Mockito.when(tagsOracleDaoMock.loadAll()).thenReturn(tags);
        
    	List<Tag> actualTags = commonFacadeServiceImpl.getTagsList();
        
    	Mockito.verify(tagsOracleDaoMock, Mockito.times(1)).loadAll(); 
        Assert.assertEquals(tags, actualTags);
    }

    /**
     * Tests getNotExpiredAuthors().
     */
    @Test
    public void getNotExpiredAuthorsTest() {
    	List<Author> authors = Arrays.asList(new Author(1L, "Genry Rider"));
    	
    	Mockito.when(authorsOracleDaoMock.loadNotExpired()).thenReturn(authors);
        
    	List<Author> actualAuthors = commonFacadeServiceImpl.getNotExpiredAuthors();
    	
        Mockito.verify(authorsOracleDaoMock, Mockito.times(1)).loadNotExpired();
        Assert.assertEquals(authors, actualAuthors);
    }
}