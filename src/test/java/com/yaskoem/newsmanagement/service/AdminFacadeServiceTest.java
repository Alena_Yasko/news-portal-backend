package com.yaskoem.newsmanagement.service;

import com.yaskoem.newsmanagement.dao.oracle.AuthorsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.CommentsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.NewsOracleDao;
import com.yaskoem.newsmanagement.dao.oracle.TagsOracleDao;
import com.yaskoem.newsmanagement.domain.Author;
import com.yaskoem.newsmanagement.domain.News;
import com.yaskoem.newsmanagement.domain.NewsTO;
import com.yaskoem.newsmanagement.domain.Tag;
import com.yaskoem.newsmanagement.service.impl.AdminFacadeServiceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * JUnit test class for {@link AdminFacadeServiceImpl}.
 * @author Alena_Yasko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
public class AdminFacadeServiceTest {

	/** News DAO mock */
    @Mock
    private NewsOracleDao newsOracleDaoMock;

    /** Authors DAO mock */
    @Mock
    private AuthorsOracleDao authorsOracleDaoMock;

    /** Tags DAO mock */
    @Mock
    private TagsOracleDao tagsOracleDaoMock;

    /** Comments DAO mock */
    @Mock
    private CommentsOracleDao commentsOracleDaoMock;

    /** Tested class */
    @InjectMocks
    private AdminFacadeServiceImpl adminFacadeServiceImpl;

    /**
     * Initialization.
     */
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests addNews(NewsTO).
     */
    @Test
    public void addNewsTest() {
        long newsId = 1L;
        News news = new News("DBUnit news title", "Short text", "Full text");
        long authorId = 2L;
        Author author = new Author(authorId, "Patrick Swayze");
        
        List<Tag> tags = Arrays.asList(new Tag(3L, "literature"), new Tag(4L, "health"));
        List<Long> tagsIds = Arrays.asList(3L, 4L); 
        
        NewsTO newsTO = new NewsTO(news, author, tags);

        Mockito.when(newsOracleDaoMock.add(news)).thenReturn(newsId);
        
        Mockito.doNothing().when(authorsOracleDaoMock).bindToNews(newsId, authorId);
        Mockito.when(tagsOracleDaoMock.loadByNewsId(newsId)).thenReturn(new ArrayList<Tag>());
        Mockito.doNothing().when(tagsOracleDaoMock).bindToNews(Mockito.eq(newsId), Mockito.anyLong());
        
        adminFacadeServiceImpl.addNews(newsTO);

        Mockito.verify(newsOracleDaoMock, Mockito.times(1)).add(news);
        Mockito.verify(authorsOracleDaoMock, Mockito.times(1)).bindToNews(newsId, authorId);
        Mockito.verify(tagsOracleDaoMock, Mockito.times(1)).loadByNewsId(newsId);
        Mockito.verify(tagsOracleDaoMock, Mockito.never()).unbindFromNews(Mockito.eq(newsId), Mockito.anyLong());
        
        ArgumentCaptor<Long> tagsIdsCaptor = ArgumentCaptor.forClass(Long.class);
        Mockito.verify(tagsOracleDaoMock, Mockito.times(2)).bindToNews(Mockito.eq(newsId), tagsIdsCaptor.capture());
        
        Assert.assertEquals(tagsIds.size(), tagsIdsCaptor.getAllValues().size());
        for (Long tagId : tagsIds) {
        	Assert.assertTrue(tagsIdsCaptor.getAllValues().contains(tagId));
        }
    }

    /**
     * Tests updateNews(NewsTO).
     */
    @Test
    public void updateNewsTest() {
    	long newsId = 1L;
        News news = new News(newsId, "DBUnit news title", "Short text", "Full text");
        long authorId = 2L;
        Author author = new Author(authorId, "Patrick Swayze");
        
        List<Tag> newTags = Arrays.asList(new Tag(3L, "literature"), new Tag(4L, "health"));
        List<Long> tagsIds = Arrays.asList(3L, 4L); 
        
        NewsTO newsTO = new NewsTO(news, author, newTags);

        List<Tag> oldTags = Collections.singletonList(new Tag(5L, "literature")); 
        List<Long> oldTagsIds = Arrays.asList(5L); 
        
        Mockito.doNothing().when(newsOracleDaoMock).update(news);
        Mockito.doNothing().when(authorsOracleDaoMock).unbindFromNews(newsId);
        Mockito.doNothing().when(authorsOracleDaoMock).bindToNews(newsId, authorId);
        Mockito.when(tagsOracleDaoMock.loadByNewsId(newsId)).thenReturn(oldTags);
        Mockito.doNothing().when(tagsOracleDaoMock).unbindFromNews(Mockito.eq(newsId), Mockito.anyLong());
        Mockito.doNothing().when(tagsOracleDaoMock).bindToNews(Mockito.eq(newsId), Mockito.anyLong());
       
        adminFacadeServiceImpl.updateNews(newsTO);

        Mockito.doNothing().when(tagsOracleDaoMock).unbindFromNews(Mockito.eq(newsId), Mockito.anyLong());
        Mockito.doNothing().when(tagsOracleDaoMock).bindToNews(Mockito.eq(newsId), Mockito.anyLong());
        
        Mockito.verify(newsOracleDaoMock, Mockito.times(1)).update(news);
        Mockito.verify(authorsOracleDaoMock, Mockito.times(1)).unbindFromNews(newsId);
        Mockito.verify(authorsOracleDaoMock, Mockito.times(1)).bindToNews(newsId, authorId);
        Mockito.verify(tagsOracleDaoMock, Mockito.times(1)).loadByNewsId(newsId);
        
        ArgumentCaptor<Long> unboundTagsIds = ArgumentCaptor.forClass(Long.class);
        
        Mockito.verify(tagsOracleDaoMock, Mockito.times(oldTagsIds.size())).unbindFromNews(Mockito.eq(newsId), unboundTagsIds.capture());
        Assert.assertEquals(oldTagsIds.size(), unboundTagsIds.getAllValues().size());
        
        for(Long tagId : oldTagsIds) {
        	Assert.assertTrue(unboundTagsIds.getAllValues().contains(tagId));
        } 
        
        ArgumentCaptor<Long> boundTagsIds = ArgumentCaptor.forClass(Long.class);
        
        Mockito.verify(tagsOracleDaoMock, Mockito.times(tagsIds.size())).bindToNews(Mockito.eq(newsId), boundTagsIds.capture());
        Assert.assertEquals(tagsIds.size(), boundTagsIds.getAllValues().size());
        
        for(Long tagId : tagsIds) {
        	Assert.assertTrue(boundTagsIds.getAllValues().contains(tagId));
        } 
    }

    /**
     * Tests deleteNews(long).
     */
    @Test
    public void deleteNewsTest() {
        Long newsId = 1L;

        Mockito.doNothing().when(authorsOracleDaoMock).unbindFromNews(newsId);
        Mockito.doNothing().when(tagsOracleDaoMock).unbindAllFromNews(newsId);
        Mockito.doNothing().when(commentsOracleDaoMock).deleteAllByNewsId(newsId);
        Mockito.doNothing().when(newsOracleDaoMock).delete(newsId);
        		
        adminFacadeServiceImpl.deleteNews(newsId);
        
        ArgumentCaptor<Long> newsIdCaptor = ArgumentCaptor.forClass(Long.class);
        
        Mockito.verify(authorsOracleDaoMock, Mockito.times(1)).unbindFromNews(newsIdCaptor.capture());
        Assert.assertEquals(newsId, newsIdCaptor.getValue());
        
        Mockito.verify(tagsOracleDaoMock, Mockito.times(1)).unbindAllFromNews(newsIdCaptor.capture());
        Assert.assertEquals(newsId, newsIdCaptor.getValue());
        
        Mockito.verify(commentsOracleDaoMock, Mockito.times(1)).deleteAllByNewsId(newsIdCaptor.capture());
        Assert.assertEquals(newsId, newsIdCaptor.getValue());
        
        Mockito.verify(newsOracleDaoMock, Mockito.times(1)).delete(newsIdCaptor.capture());
        Assert.assertEquals(newsId, newsIdCaptor.getValue());
    }

    /**
     * Tests deleteComment(long).
     */
    @Test
    public void deleteCommentTest() {
    	Long commentId = 1L;
    	
    	Mockito.doNothing().when(commentsOracleDaoMock).delete(commentId);
    	
        adminFacadeServiceImpl.deleteComment(commentId);

    	ArgumentCaptor<Long> commentIdCaptor = ArgumentCaptor.forClass(Long.class);
        Mockito.verify(commentsOracleDaoMock, Mockito.times(1)).delete(commentIdCaptor.capture());
        
        Assert.assertEquals(commentId, commentIdCaptor.getValue());
    }

    /**
     * Tests addTag(Tag).
     */
    @Test
    public void addTagTest() {
        Tag tag = new Tag("Test tag");
        Long newTagId = 1L;
        
        Mockito.when(tagsOracleDaoMock.add(tag)).thenReturn(newTagId);
        adminFacadeServiceImpl.addTag(tag);
        
        ArgumentCaptor<Tag> tagCaptor = ArgumentCaptor.forClass(Tag.class);
        Mockito.verify(tagsOracleDaoMock, Mockito.times(1)).add(tagCaptor.capture());
        Assert.assertEquals(tag,  tagCaptor.getValue());
    }

    /**
     * Tests addAuthor(Author).
     */
    @Test
    public void addAuthorTest() {
        Author author = new Author("Test author");
        long newAuthorId = 1L;
        
        Mockito.when(authorsOracleDaoMock.add(author)).thenReturn(newAuthorId);
        adminFacadeServiceImpl.addAuthor(author);
        
        ArgumentCaptor<Author> authorCaptor = ArgumentCaptor.forClass(Author.class);
        Mockito.verify(authorsOracleDaoMock, Mockito.times(1)).add(authorCaptor.capture());
        Assert.assertEquals(author, authorCaptor.getValue());
    }
}