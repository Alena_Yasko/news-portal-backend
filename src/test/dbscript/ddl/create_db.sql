CREATE USER NEWS IDENTIFIED BY password;
GRANT ALL PRIVILEGES TO NEWS;

CREATE TABLE NEWS
( ns_news_id_pk NUMBER(20) NOT NULL,
  ns_title NVARCHAR2(30) NOT NULL,
  ns_short_text NVARCHAR2(100) NOT NULL,
  ns_full_text NVARCHAR2(2000) NOT NULL,
  ns_creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  ns_modification_date DATE DEFAULT CURRENT_DATE NOT NULL,
  CONSTRAINT pk_news PRIMARY KEY (ns_news_id_pk)
);

CREATE SEQUENCE news_pk_sequence;

CREATE OR REPLACE TRIGGER NEWS_BIS_TRG
BEFORE INSERT ON NEWS
FOR EACH ROW
BEGIN
  IF :NEW.ns_news_id_pk IS NULL THEN
    SELECT news_pk_sequence.NEXTVAL
    INTO   :NEW.ns_news_id_pk
    FROM   DUAL;
    END IF;
END;
/

CREATE TABLE AUTHORS
( au_author_id_pk NUMBER(20) NOT NULL,
  au_author_name_uq NVARCHAR2(30) NOT NULL,
  au_expired TIMESTAMP DEFAULT NULL,
  CONSTRAINT pk_authors PRIMARY KEY (au_author_id_pk),
  CONSTRAINT uq_authors_name UNIQUE (au_author_name_uq)
);

CREATE SEQUENCE authors_pk_sequence;

CREATE OR REPLACE TRIGGER AUTHORS_BIS_TRG
BEFORE INSERT ON AUTHORS
FOR EACH ROW
BEGIN
  if :NEW.au_author_id_pk IS NULL THEN
    SELECT authors_pk_sequence.NEXTVAL
    INTO   :NEW.au_author_id_pk
    FROM   DUAL;
  END IF;
END;
/

CREATE TABLE NEWS_AUTHORS
( na_news_id_fk NUMBER(20) NOT NULL,
  na_author_id_fk NUMBER(20) NOT NULL,
  CONSTRAINT na_news_fk
    FOREIGN KEY (na_news_id_fk)
    REFERENCES NEWS(ns_news_id_pk),
  CONSTRAINT na_authors_fk
    FOREIGN KEY (na_author_id_fk)
    REFERENCES AUTHORS(au_author_id_pk),
  CONSTRAINT uq_na_news_id UNIQUE (na_news_id_fk)
);

CREATE TABLE COMMENTS
( cm_comment_id_pk NUMBER(20) NOT NULL,
  cm_news_id_fk NUMBER(20) NOT NULL,
  cm_comment_text NVARCHAR2(100) NOT NULL,
  cm_creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT pk_comments PRIMARY KEY (cm_comment_id_pk),
  CONSTRAINT cm_news_fk
    FOREIGN KEY (cm_news_id_fk)
    REFERENCES NEWS(ns_news_id_pk)
);

CREATE SEQUENCE comments_pk_sequence;

CREATE OR REPLACE TRIGGER COMMENTS_BIS_TRG
BEFORE INSERT ON COMMENTS
FOR EACH ROW
BEGIN
  IF :NEW.cm_comment_id_pk IS NULL THEN
    SELECT comments_pk_sequence.NEXTVAL
    INTO   :NEW.cm_comment_id_pk
    FROM   DUAL;
  END IF;
END;
/

CREATE TABLE ROLES
( rl_role_id_pk NUMBER(20) NOT NULL,
  rl_role_name_uq VARCHAR2(50) NOT NULL,
  CONSTRAINT pk_roles PRIMARY KEY (rl_role_id_pk),
  CONSTRAINT uq_roles_name UNIQUE (rl_role_name_uq)
);

CREATE SEQUENCE roles_pk_sequence;

CREATE OR REPLACE TRIGGER ROLES_BIS_TRG
BEFORE INSERT ON ROLES
FOR EACH ROW
BEGIN
  IF :NEW.rl_role_id_pk IS NULL THEN
    SELECT roles_pk_sequence.NEXTVAL
    INTO   :NEW.rl_role_id_pk
    FROM   DUAL;
  END IF;
END;
/

CREATE TABLE USERS
( us_user_id_pk NUMBER(20) NOT NULL,
  us_user_name NVARCHAR2(50) NOT NULL,
  us_login_uq VARCHAR2(30) NOT NULL,
  us_password VARCHAR2(32) NOT NULL,
  us_role_id_fk NUMBER(20) NOT NULL,
  CONSTRAINT pk_users PRIMARY KEY (us_user_id_pk),
  CONSTRAINT us_roles_fk
    FOREIGN KEY (us_role_id_fk)
    REFERENCES ROLES(rl_role_id_pk),
  CONSTRAINT uq_users_login UNIQUE (us_login_uq)
);

CREATE SEQUENCE users_pk_sequence;

CREATE OR REPLACE TRIGGER USERS_BIS_TRG
BEFORE INSERT ON USERS
FOR EACH ROW
BEGIN
  IF :NEW.us_user_id_pk IS NULL THEN
    SELECT users_pk_sequence.NEXTVAL
    INTO   :NEW.us_user_id_pk
    FROM   DUAL;
  END IF;
END;
/

CREATE TABLE TAGS
( tg_tag_id_pk NUMBER(20) NOT NULL,
  tg_tag_name_uq NVARCHAR2(30) NOT NULL,
  CONSTRAINT pk_tags PRIMARY KEY (tg_tag_id_pk),
  CONSTRAINT uq_tags_name UNIQUE (tg_tag_name_uq)
);

CREATE SEQUENCE tags_pk_sequence;

CREATE OR REPLACE TRIGGER TAGS_BIS_TRG
BEFORE INSERT ON TAGS
FOR EACH ROW
BEGIN
  IF :NEW.tg_tag_id_pk IS NULL THEN
    SELECT tags_pk_sequence.NEXTVAL
    INTO   :NEW.tg_tag_id_pk
    FROM   DUAL;
  END IF;
END;
/

CREATE TABLE NEWS_TAGS
( nt_news_id_fk NUMBER(20) NOT NULL,
  nt_tag_id_fk NUMBER(20) NOT NULL,
  CONSTRAINT nt_news_fk
    FOREIGN KEY (nt_news_id_fk)
    REFERENCES NEWS(ns_news_id_pk),
  CONSTRAINT nt_tags_fk
    FOREIGN KEY (nt_tag_id_fk)
    REFERENCES TAGS(tg_tag_id_pk)
);