INSERT ALL
   INTO ROLES (rl_role_name_uq) VALUES ('ROLE_ADMIN')
   INTO ROLES (rl_role_name_uq) VALUES ('ROLE_USER')
SELECT 1 FROM DUAL;

INSERT ALL
   INTO TAGS (tg_tag_name_uq) VALUES ('science')
   INTO TAGS (tg_tag_name_uq) VALUES ('music')
   INTO TAGS (tg_tag_name_uq) VALUES ('art')
   INTO TAGS (tg_tag_name_uq) VALUES ('technologies')
   INTO TAGS (tg_tag_name_uq) VALUES ('sport')
   INTO TAGS (tg_tag_name_uq) VALUES ('celebrities')
   INTO TAGS (tg_tag_name_uq) VALUES ('movies')
   INTO TAGS (tg_tag_name_uq) VALUES ('breaking')
   INTO TAGS (tg_tag_name_uq) VALUES ('india')
   INTO TAGS (tg_tag_name_uq) VALUES ('africa')
   INTO TAGS (tg_tag_name_uq) VALUES ('syria')
   INTO TAGS (tg_tag_name_uq) VALUES ('russia')
   INTO TAGS (tg_tag_name_uq) VALUES ('entertainment')
   INTO TAGS (tg_tag_name_uq) VALUES ('literature')
   INTO TAGS (tg_tag_name_uq) VALUES ('health')
   INTO TAGS (tg_tag_name_uq) VALUES ('canada')
   INTO TAGS (tg_tag_name_uq) VALUES ('nature')
   INTO TAGS (tg_tag_name_uq) VALUES ('food')
   INTO TAGS (tg_tag_name_uq) VALUES ('humour')
   INTO TAGS (tg_tag_name_uq) VALUES ('faq')
SELECT 1 FROM DUAL;

INSERT ALL
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('John Smith', 'smith', '5f4dcc3b5aa765d61d8327deb882cf99', '1')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Anna Ryabova', 'annie', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Adwin Smith', 'adwin', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Jane Trooper', 'jenny', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('William White', 'will_white', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Kell Dilan', 'dilan', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Robert Louis', 'bob77', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('John Voyt', 'jv78', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('John Doe', 'anonymous', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Alan Welsh', 'alan_v', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Dmitro Komarov', 'komarov', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Chao Ling', 'ling_chao', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Ivan Smirnov', 'smirnov', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Clara Rainolds', 'blondie', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Jessica Gonzalez', 'jess74', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Simon Neil', 'big_guitar', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Kate Morgan', 'kitty', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Adam Savage', 'adam3', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Kelly Potter', 'kelly', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
  INTO USERS (us_user_name, us_login_uq, us_password, us_role_id_fk)
    VALUES ('Alex Brown', 'brown82', '5f4dcc3b5aa765d61d8327deb882cf99', '2')
SELECT 1 FROM DUAL;

INSERT ALL
  INTO AUTHORS (au_author_name_uq)
    VALUES ('John Smith')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Anna Ryabova')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Adwin Smith')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Jane Trooper')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('William White')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Kell Dilan')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Robert Louis')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('John Voyt')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('John Doe')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Alan Welsh')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Dmitro Komarov')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Chao Ling')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Ivan Smirnov')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Clara Rainolds')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Jessica Gonzalez')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Simon Neil')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Kate Morgan')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Adam Savage')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Kelly Potter')
  INTO AUTHORS (au_author_name_uq)
    VALUES ('Alex Brown')
SELECT 1 FROM DUAL;

INSERT ALL
INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('The biggest radio telescope',
        'Nestling in a vast natural crater, China’s giant is about to come alive.',
        'A colossal, steeply curved dish glints in the sunlight, surrounded by jagged
        mountains that cut into the sky. Construction workers, busy putting the finishing
        touches to this structure, look tiny against the huge backdrop. This is the largest
        radio telescope ever built, measuring 500m (1,640ft) across.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('The particle hunt',
        'At Daya Bay scientists are studying some of the oddest particles - neutrinos.',
        'Neutrinos are generated by nuclear reactions - from the fusion in stars, to
        nuclear power stations here on Earth - and are one of the most abundant particles
        in the Universe. Trillions of neutrinos pass through us every second, but we
        cannot feel them or see them - they have no charge and barely a hint of mass.
        They’ve been described as being as close to nothing as something can get.
        But stranger than all of this is the way neutrinos are constantly changing.
        As they travel through the Universe, they switch between three different forms - or
        "flavours", as scientists put it.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Race to the deep',
        'In a shipyard in Shanghai music is playing and giant balloons are whipping about in the wind.',
        'A huge crowd has gathered to watch China’s new scientific research ship enter the
        water for the first time. And after the obligatory bottle of champagne is smashed,
        the tethers are cut and the 100m-long vessel rolls into the water. This ship,
        equipped with on-board labs and the latest scientific kit, will eventually explore
        the world’s oceans. But it is also going to help China plunge beneath the waves:
        it will serve as a launch-pad for submarines that can dive to the deepest parts
        of the ocean.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Open space',
        'China''s space program has certainly captured the imagination.',
        'At Beijing''s science and technology museum children are running around,
        clambering inside a model of a space station, taking miniature rovers for a Test
        drive and even having a go on a spinning gyroscope. "Space is fun and very cool,"
        one little boy tells me. I ask him about his ambitions, and he pauses for a moment
        before deciding yes, he would very much like to head into space and become a Chinese
        astronaut - or taikonaut - when he grows up.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('How the Irish lost their words',
        'Meet the man who''s charged with getting Ireland talking again',
        'The stories my uncle told were the everyday made interesting. They were anecdotes
        about the butcher or the bus driver, or a screaming match at the end of the street.
        Every story was true, but embellished each time it was told; embroidered to make the
        story more entertaining. It’s a way of telling stories that is very Irish. You
        probably know it better as blarney. Jack Lynch prefers to call them "tall tales".
        As the Chair of Aos Scéal Éireann, or Storytellers of Ireland, Lynch is the man
        charged with getting the Irish talking again. Incredibly, it seems that we had stopped.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Ready to welcome the world',
        'For more than 500 years, the only way to reach the remote South Atlantic island was by sea.',
        'Travelling to the British Overseas Territory of Saint Helena by sailboat, after a
        nine-day voyage from Namibia, my family and I made landfall the way every person before
        us has: the way Napoleon Bonaparte did when he was sent into exile in 1815; the way
        modern-day Saints (as the local population is known) do when they venture home from work
        in the UK; and the way the occasional, intrepid visitor has always done. But we were one
        of the last travellers to do so.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Taste of water',
        'The man is paid to taste water.',
        'Sommelier Martin Riese tastes water for a living at a Los Angeles restaurant, creating
        different menus from his collection of 21 spring and mineral waters.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('The empire the world forgot',
        'The former regional power of Ani is now an eerie, abandoned city of ghosts.',
        'The city of Ani once housed many thousands of people, becoming a cultural hub and regional
        power under the medieval Bagratid Armenian dynasty. Today, it’s an eerie, abandoned city of
        ghosts that stands alone on a plateau in the remote highlands of northeast Turkey, 45km away
        from the Turkish border city of Kars. As you walk among the many ruins, left to deteriorate
        for over 90 years, the only sound is the wind howling through a ravine that marks the border
        between Turkey and Armenia.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('The abandoned mansions',
        'The barren landscapes of Thar Desert were once home to the opulence of billionaires',
        'Founded by the eponymous Rajput chieftain Rao Shekha in the late 15th Century, Shekhawati
        prospered immensely at the turn of the 19th Century. The region reduced taxes to lure merchants
        and diverted all caravan trade from the nearby commercial centres of Jaipur and Bikaner. Merchants
        belonging to the Marwari and Bania community, a renowned ethnic trading group in India, moved into
        Shekhawati from the surrounding towns, and amassed great wealth through a  flourishing trade in
        opium, cotton and spices. Modest merchant homes started giving way to grand mansions by the end of
        the 19th Century.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('50 Reasons to love the World',
        'Headlines can paint a pretty grim picture of life across our planet.',
        'Spin the globe. Pack a bag. Break bread with strangers. Soak in radical beauty. In short – travel.
        We asked a range of people, from writers and chefs to musicians and photographers, to share one
        experience from the last year that truly inspired them – something that, in no uncertain terms,
        reminded them why they love the world.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Tea made with cow dung',
        'Chai wallahs brew tea with milk only minutes old! Now that is fresh.',
        'Varanasi brings a totally new flavour to the British afternoon ritual.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('One of most dangerous roads',
        'Allured by the promise of traversing one of India’s most breathtaking and most dangerous roads.',
        'The Pangi via Kishtwar road cuts rights through mountain ledges, and for the most part, is
        only wide enough for one car at a time. Somehow though, brave bus and truck drivers ply the
        route too. If two cars meet each other going opposite directions, one driver has to carefully
        drive in reverse for potentially hundreds of metres until a suitable passing spot is reached.
        All along the road, cliffs shoot straight up to the tops of mountains, and then straight down
        thousands of metres to the rubble-strewn banks of the Chenab River. The road is so rocky and
        steep that one 30km stretch took us four hours to cross.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Hauntingly beautiful disaster',
        'The abandoned city of Pripyat 30 years after the Chernobyl disaster.',
        'During my four days in Pripyat, I had the opportunity to stay in the city after sundown
        (usually, visitors have to leave around 4 pm). Earlier that afternoon, I’d admired the sunlight
        pouring through the beautiful glasswork of this former cafe, and it gave me the idea to recreate
        the scene by artificially illuminating the building from the inside using a light painting
        technique and technical long exposures. Early one morning, I watched the sunrise from the top
        of Pripyat’s highest building, the Fujiyama. The beams slowly shone through the winter fog and
        rays flooded the dead city, bringing the whole place back to life for a few short seconds.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('When Disney got trippy',
        'At one time the animation mogul’s films veered toward radical, experimental art.',
        'Futurist, surrealist, abstract artist: those are not customary descriptions of Walt Disney,
        yet they all fit. People who insist on pigeonholing him as a purveyor of bland family entertainment
        haven’t bothered to watch his movies closely, especially his work in the 1940s. Fantasia alone
        should silence nay-sayers who only see Disney as a commercial populist; 75 years after its debut
        on 13 November 1940, it remains one of the most astonishing films ever to come from Hollywood.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Parrots return to paradise',
        'For the first time captive bred scarlet macaws have been released.',
        'If you were to say ‘parrot’, most people would conjure up an image of a bird similar to the
        scarlet macaw (Ara macao), adorned with plumage containing all the three primary colours,
        topped off with large grey beak. Despite being widely recognisable (they have pirates to thank
        for that) in reality these parrots have become astonishingly rare in the wild. The scarlet
        macaw is divided into two populations, with one confined to South America, while the other
        is distributed across Central America (Ara macao cyanoptera). Under threat throughout its
        range, the subspecies that is found across Guatemala, Mexico and Belize is believed to be
        particularly vulnerable, with only 300-400 macaws now believed to be remaining in the wild.
        The birds here are the unfortunate victims of a lethal cocktail of devastating habitat loss,
        combined with an insatiable - and illegal - pet trade.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Terraces of Lake Rotomahana',
        'Obliterated by a volcanic eruption in 1886, now found hidden at the bottom of the lake.',
        'In the early hours of 10 June 1886, Mount Tarawera, a volcano on the North Island of New Zealand,
        erupted with astonishing force. The explosions may have been heard as far afield as Christchurch,
        more than 400 miles (640km) to the south-west. The eruption killed 120 people, most of them
        Maoris – native New Zealanders – living in small villages in the surrounding countryside.
        But it is not just because of its high death toll that the Tarawera eruption is firmly lodged
        in the collective memory of New Zealanders. Most people also remember the eruption because
        it robbed the island nation of a treasured natural wonder: the Pink and White Terraces of Lake
        Rotomahana. The terraces were the two largest formations of silica sinter – a fine-grained
        version of quartz – ever known to have existed on Earth. They were located on opposite shores
        of Lake Rotomahana, situated six miles (10km) to the south-west of Mount Tarawera. And they
        were extraordinarily beautiful.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Greece moves Idomeni migrants',
        'Greece has begun evacuating thousands of stranded migrants from the makeshift Idomeni camp.',
        'The operation began at dawn and witnesses reported police vehicles and buses standing by to
        transfer people to better organised facilities. Riot police have been deployed but officials
        say force will not be used. The camp sprang up in February after border closures in Macedonia
        left more than 8,000 people stranded there. The migrants, mostly from conflict zones in Syria,
        Iraq and Afghanistan, have refused to move despite having to sleep in tents in very difficult
        conditions. They have largely ignored appeals by Greek authorities to move to organised camps
        set up around Greece because they say it would move them further away from the border.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Crashed flight MS804',
        '"An EgyptAir flight that crashed in the Mediterranean did not swerve before disappearing."',
        'The Airbus A320 was en route from Paris to Cairo with 66 people aboard when it vanished
        from radar early on Thursday. Greece''s defence minister said the plane turned 90 degrees left
        and then did a 360-degree turn towards the right before plummeting. But a senior Egyptian
        aviation official said there was no unusual movement. Ehab Azmy, the head of Egypt''s state-run
        provider of air navigation services, told the Associated Press that the plane had been flying
        at its normal height of 37,000ft (11,280m) before dropping off the radar. Some debris has since
        been found. "That fact degrades what the Greeks are saying about the aircraft suddenly
        losing altitude before it vanished from radar," he said. The reason for the discrepancy between
        the Greek and Egyptian accounts of the plane crash is not clear.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Leaked tape forces Juca out',
        'A close ally of Brazilian acting President is stepping aside in a new political scandal.',
        'Planning Minister Romero Juca was caught on tape allegedly conspiring to obstruct the
        country''s biggest-ever corruption investigation. In the tapes, leaked by a newspaper,
        he appears to talk of stopping the probe at oil giant Petrobras by impeaching suspended
        President Dilma Rousseff. Mr Juca says his comments have been taken out of context.')

INTO NEWS (ns_title, ns_short_text, ns_full_text)
VALUES ('Lifting the veil',
        'Why is Bulgaria making a big fuss about the niqab?',
        'Bulgaria is set to become the latest EU country to ban the niqab, or face veil, after
        the right-wing coalition party, Patriotic Front, submitted a bill to parliament as a
        "pre-emptive measure". Belgium, France and Latvia have already done so but Bulgaria''s
        example stems from the small, southern city of Pazardjik, which has just imposed its own
        "burka ban", as local media dubbed it. The burka, which covers the eyes, has never been
        seen in Bulgaria. The face veil is not considered traditional dress for Bulgaria''s Muslims,
        who make up 10% of the country''s 7.1 million population. The vast majority are indigenous
        communities of ethnic Turks, Roma and Pomaks (Bulgarian-speaking Muslims). In fact,
        women who have been spotted wearing it in the past two to three years are almost all members
        of a small Salafist, Roma community in Pazardjik.')
SELECT 1 FROM DUAL;

INSERT ALL
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (1, 'comment1 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (1, 'comment2 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (1, 'comment3 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (1, 'comment4 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (2, 'comment5 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (2, 'comment6 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (5, 'comment7 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (5, 'comment8 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (5, 'comment9 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (6, 'comment10 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (11, 'comment11 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (11, 'comment12 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (12, 'comment13 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (14, 'comment14 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (17, 'comment15 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (17, 'comment16 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (17, 'comment17 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (18, 'comment18 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (20, 'comment19 here...')
  INTO COMMENTS (cm_news_id_fk, cm_comment_text)
    VALUES (20, 'comment20 here...')
SELECT 1 FROM DUAL;

INSERT ALL
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (1, 1)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (2, 2)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (3, 3)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (3, 4)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (3, 5)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (4, 6)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (5, 7)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (5, 8)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (5, 9)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (6, 10)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (7, 11)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (8, 12)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (9, 13)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (10, 14)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (11, 15)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (12, 16)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (13, 17)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (14, 18)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (15, 19)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (16, 13)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (17, 20)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (17, 15)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (18, 20)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (18, 15)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (19, 20)
  INTO NEWS_TAGS (nt_news_id_fk, nt_tag_id_fk)
    VALUES (20, 2)
SELECT 1 FROM DUAL;

INSERT ALL
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (1, 1)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (2, 2)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (3, 3)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (4, 4)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (5, 5)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (6, 5)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (7, 5)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (8, 5)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (9, 5)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (10, 20)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (11, 7)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (12, 12)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (13, 13)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (14, 14)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (15, 15)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (16, 16)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (17, 18)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (18, 18)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (19, 19)
  INTO NEWS_AUTHORS (na_news_id_fk, na_author_id_fk)
    VALUES (20, 20)
SELECT 1 FROM DUAL;
